﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Stretch del contrasto", Category = "FEI")]
	public class ContrastStretch : ImageOperation<Image<byte>, Image<byte>> {
		private int stretchDiscardPerc;

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int StretchDiscardPercentage {
			get { return stretchDiscardPerc; }
			set {
				if (value < 0 || value > 100)
					throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 100.");
				stretchDiscardPerc = value;
			}
		}

		public ContrastStretch() {
		}

		public ContrastStretch(Image<byte> inputImage)
			: base(inputImage) {
			StretchDiscardPercentage = 0;
		}

		public ContrastStretch(Image<byte> inputImage, int stretchDiscard)
			: base(inputImage) {
			StretchDiscardPercentage = stretchDiscard;
		}

		public override void Run() {
			//Calcolo il numero di pixel da scartare
			int nDiscardPixel = (StretchDiscardPercentage * InputImage.PixelCount) / 100;

			//Setto il minimo e il massimo
			int min = 0;
			int max = 255;
			int sum;

			//Creo l'istogramma
			Histogram histogram = new HistogramBuilder(InputImage).Execute();

			//Posizione il mio contatore all'inizio, sul lato sinistro, dell'istogramma
			sum = histogram[min];

			/*
			 * Scorro tutto il vettore in avanti fintanto che min<255 e ho scartato la percentuale giusta di pixel a sinistra.
			 * Vado in sostanza a riposizionare correttamente la variabile min.		
			 * Nota che uso sum += histogram[++min]; perchè ogni cella dell'istogramma al suo interno ha un numero di pixel quindi io devo 
			 * tenere traccia di quanti ne scarto confrontado ogni volta con nDiscardPixel
			 */
			while ((min < 255) && (sum <= nDiscardPixel)) {
				sum += histogram[++min];
			}

			//Posiziono il mio contatore ora sul lato destro dell'istogramma 
			sum = histogram[max];

			//Ora procedo all'indietro fintanto che max>0 e ho scartato la percentuale giusta di pixel a destra
			//Vado ora a riposizionare la variabile max
			while ((max > 0) && (sum <= nDiscardPixel)) {
				sum += histogram[--max];
			}

			int diff = max - min;
			if (diff > 0) {
				var op = new LookupTableTransform<byte>(InputImage, p => (255 * (p - min) / diff).ClipToByte());
				Result = op.Execute();
			} else {
				Result = InputImage.Clone();
			}
		}
	}

	[AlgorithmInfo("Equalizzazione istogramma", Category = "FEI")]
	public class HistogramEqualization : ImageOperation<Image<byte>, Image<byte>> {
		public HistogramEqualization() {
		}

		public HistogramEqualization(Image<byte> inputImage)
			: base(inputImage) {
		}

		public override void Run() {
			// Calcola l'istogramma
			var hist = new HistogramBuilder(InputImage).Execute();

			// Ricalcola ogni elemento dell'istogramma come somma dei precedenti
			for (int i = 1; i < 256; i++)
				hist[i] += hist[i - 1];

			// Definisce la funzione di mapping e applica la LUT
			var op = new LookupTableTransform<byte>(InputImage, p => (byte)(255 * hist[p] / InputImage.PixelCount));
			Result = op.Execute();
		}
	}

	[AlgorithmInfo("Operazione aritmetica Grayscale", Category = "FEI")]
	public class ImageArithmetic : ImageOperation<Image<byte>, Image<byte>, Image<byte>> {
		[AlgorithmParameter]
		[DefaultValue(defaultOperation)]
		public ImageArithmeticOperation Operation { get; set; }
		const ImageArithmeticOperation defaultOperation = ImageArithmeticOperation.Difference;

		public ImageArithmetic() {
		}

		public ImageArithmetic(Image<byte> image1, Image<byte> image2, ImageArithmeticOperation operation)
			: base(image1, image2) {
			Operation = operation;
		}

		public ImageArithmetic(Image<byte> image1, Image<byte> image2)
			: this(image1, image2, defaultOperation) {
		}

		public override void Run() {
			if ((InputImage1.Width == InputImage2.Width) && (InputImage1.Height == InputImage2.Height)) {
				Result = new Image<byte>(InputImage1.Width, InputImage1.Height);
				switch (Operation) {
					case ImageArithmeticOperation.Add:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var newPixel = InputImage1[i] + InputImage2[i];

							if (newPixel > 255)
								Result[i] = 255;
							else
								Result[i] = newPixel.ClipToByte();
						}
						break;

					case ImageArithmeticOperation.And:
						for (int i = 0; i < InputImage1.PixelCount; i++)
							Result[i] = (byte)(InputImage1[i] & InputImage2[i]);
						break;

					case ImageArithmeticOperation.Average:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							int newPixel = (int)(InputImage1[i] + InputImage2[i]) / 2;

							if (newPixel > 255)
								Result[i] = 255;
							else
								Result[i] = newPixel.ClipToByte();
						}
						break;

					case ImageArithmeticOperation.Darkest:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							if (InputImage1[i] < InputImage2[i])
								Result[i] = InputImage1[i];
							else
								Result[i] = InputImage2[i];
						}
						break;

					case ImageArithmeticOperation.Difference:
						for (int i = 0; i < InputImage1.PixelCount; i++)
							Result[i] = Math.Abs(InputImage1[i] - InputImage2[i]).ClipToByte();
						break;

					case ImageArithmeticOperation.Lightest:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							if (InputImage1[i] > InputImage2[i])
								Result[i] = InputImage1[i];
							else
								Result[i] = InputImage2[i];
						}
						break;

					case ImageArithmeticOperation.Or:
						for (int i = 0; i < InputImage1.PixelCount; i++)
							Result[i] = (InputImage1[i] | InputImage2[i]).ClipToByte();
						break;

					case ImageArithmeticOperation.Subtract:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var newPixel = InputImage1[i] - InputImage2[i];

							if (newPixel < 0)
								Result[i] = 0;
							else
								Result[i] = newPixel.ClipToByte();
						}
						break;

					case ImageArithmeticOperation.Xor:
						for (int i = 0; i < InputImage1.PixelCount; i++)
							Result[i] = (InputImage1[i] ^ InputImage2[i]).ClipToByte();
						break;
				}
			}
		}
	}

	[AlgorithmInfo("Operazione aritmetica RGB", Category = "FEI")]
	public class RgbImageArithmetic : ImageOperation<RgbImage<byte>, RgbImage<byte>, RgbImage<byte>> {
		[AlgorithmParameter]
		[DefaultValue(defaultOperationRGB)]
		public ImageArithmeticOperation OperationRGB { get; set; }
		const ImageArithmeticOperation defaultOperationRGB = ImageArithmeticOperation.Difference;

		public RgbImageArithmetic() {
		}

		public RgbImageArithmetic(RgbImage<byte> image1, RgbImage<byte> image2, ImageArithmeticOperation operation)
			: base(image1, image2) {
			OperationRGB = operation;
		}

		public RgbImageArithmetic(RgbImage<byte> image1, RgbImage<byte> image2)
			: this(image1, image2, defaultOperationRGB) {
		}

		public override void Run() {
			if ((InputImage1.Width == InputImage2.Width) && (InputImage1.Height == InputImage2.Height)) {
				Result = new RgbImage<byte>(InputImage1.Width, InputImage1.Height);
				switch (OperationRGB) {
					case ImageArithmeticOperation.Add:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var redPixel = InputImage1.RedChannel[i] + InputImage2.RedChannel[i];
							var greenPixel = InputImage1.GreenChannel[i] + InputImage2.GreenChannel[i];
							var bluePixel = InputImage1.BlueChannel[i] + InputImage2.BlueChannel[i];

							if (redPixel > 255)
								redPixel = 255;

							if (greenPixel > 255)
								greenPixel = 255;

							if (bluePixel > 255)
								bluePixel = 255;

							Result[i] = new RgbPixel<byte>(redPixel.ClipToByte(), greenPixel.ClipToByte(), bluePixel.ClipToByte());
						}
						break;

					case ImageArithmeticOperation.And:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var redPixel = (byte)InputImage1.RedChannel[i] & InputImage2.RedChannel[i];
							var greenPixel = (byte)InputImage1.GreenChannel[i] & InputImage2.GreenChannel[i];
							var bluePixel = (byte)InputImage1.BlueChannel[i] & InputImage2.BlueChannel[i];

							if (redPixel > 255)
								redPixel = 255;

							if (greenPixel > 255)
								greenPixel = 255;

							if (bluePixel > 255)
								bluePixel = 255;

							Result[i] = new RgbPixel<byte>(redPixel.ClipToByte(), greenPixel.ClipToByte(), bluePixel.ClipToByte());
						}
						break;

					case ImageArithmeticOperation.Average:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var redPixel = (int)(InputImage1.RedChannel[i] + InputImage2.RedChannel[i]) / 2;
							var greenPixel = (int)(InputImage1.GreenChannel[i] + InputImage2.GreenChannel[i]) / 2;
							var bluePixel = (int)(InputImage1.BlueChannel[i] + InputImage2.BlueChannel[i]) / 2;

							Result[i] = new RgbPixel<byte>(redPixel.ClipToByte(), greenPixel.ClipToByte(), bluePixel.ClipToByte());
						}
						break;

					case ImageArithmeticOperation.Darkest:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							if (InputImage1.RedChannel[i] < InputImage2.RedChannel[i])
								Result.RedChannel[i] = InputImage1.RedChannel[i];
							else
								Result.RedChannel[i] = InputImage2.RedChannel[i];

							if (InputImage1.GreenChannel[i] < InputImage2.GreenChannel[i])
								Result.GreenChannel[i] = InputImage1.GreenChannel[i];
							else
								Result.GreenChannel[i] = InputImage2.GreenChannel[i];

							if (InputImage1.BlueChannel[i] < InputImage2.BlueChannel[i])
								Result.BlueChannel[i] = InputImage1.BlueChannel[i];
							else
								Result.BlueChannel[i] = InputImage2.BlueChannel[i];
						}
						break;

					case ImageArithmeticOperation.Difference:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							Result.RedChannel[i] = Math.Abs(InputImage1.RedChannel[i] - InputImage2.RedChannel[i]).ClipToByte();
							Result.GreenChannel[i] = Math.Abs(InputImage1.GreenChannel[i] - InputImage2.GreenChannel[i]).ClipToByte();
							Result.BlueChannel[i] = Math.Abs(InputImage1.BlueChannel[i] - InputImage2.BlueChannel[i]).ClipToByte();
						}
						break;

					case ImageArithmeticOperation.Lightest:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							if (InputImage1.RedChannel[i] > InputImage2.RedChannel[i])
								Result.RedChannel[i] = InputImage1.RedChannel[i];
							else
								Result.RedChannel[i] = InputImage2.RedChannel[i];

							if (InputImage1.GreenChannel[i] > InputImage2.GreenChannel[i])
								Result.GreenChannel[i] = InputImage1.GreenChannel[i];
							else
								Result.GreenChannel[i] = InputImage2.GreenChannel[i];

							if (InputImage1.BlueChannel[i] > InputImage2.BlueChannel[i])
								Result.BlueChannel[i] = InputImage1.BlueChannel[i];
							else
								Result.BlueChannel[i] = InputImage2.BlueChannel[i];
						}
						break;

					case ImageArithmeticOperation.Or:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var redPixel = (byte)InputImage1.RedChannel[i] | InputImage2.RedChannel[i];
							var greenPixel = (byte)InputImage1.GreenChannel[i] | InputImage2.GreenChannel[i];
							var bluePixel = (byte)InputImage1.BlueChannel[i] | InputImage2.BlueChannel[i];

							if (redPixel > 255)
								redPixel = 255;

							if (greenPixel > 255)
								greenPixel = 255;

							if (bluePixel > 255)
								bluePixel = 255;

							Result[i] = new RgbPixel<byte>(redPixel.ClipToByte(), greenPixel.ClipToByte(), bluePixel.ClipToByte());
						}
						break;

					case ImageArithmeticOperation.Subtract:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var redPixel = (byte)InputImage1.RedChannel[i] - InputImage2.RedChannel[i];
							var greenPixel = (byte)InputImage1.GreenChannel[i] - InputImage2.GreenChannel[i];
							var bluePixel = (byte)InputImage1.BlueChannel[i] - InputImage2.BlueChannel[i];

							if (redPixel < 0)
								Result.RedChannel[i] = 0;
							else
								Result.RedChannel[i] = redPixel.ClipToByte();

							if (greenPixel < 0)
								Result.GreenChannel[i] = 0;
							else
								Result.GreenChannel[i] = greenPixel.ClipToByte();

							if (bluePixel < 0)
								Result.BlueChannel[i] = 0;
							else
								Result.BlueChannel[i] = bluePixel.ClipToByte();
						}
						break;

					case ImageArithmeticOperation.Xor:
						for (int i = 0; i < InputImage1.PixelCount; i++) {
							var redPixel = (byte)InputImage1.RedChannel[i] ^ InputImage2.RedChannel[i];
							var greenPixel = (byte)InputImage1.GreenChannel[i] ^ InputImage2.GreenChannel[i];
							var bluePixel = (byte)InputImage1.BlueChannel[i] ^ InputImage2.BlueChannel[i];

							if (redPixel > 255)
								redPixel = 255;

							if (greenPixel > 255)
								greenPixel = 255;

							if (bluePixel > 255)
								bluePixel = 255;

							Result[i] = new RgbPixel<byte>(redPixel.ClipToByte(), greenPixel.ClipToByte(), bluePixel.ClipToByte());
						}
						break;
				}
			}
		}
	}
}
