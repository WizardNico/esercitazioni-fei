﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;
using BioLab.PatternRecognition.Localization;
using System.Diagnostics;
namespace PRLab.FEI {

	[AlgorithmInfo("Correlazione", Category = "FEI")]
	[CustomAlgorithmPreviewOutput(typeof(OuputViewerCorrelazione))]
	public class Correlazione : ImageOperation<Image<byte>, Image<int>> {

		[AlgorithmInput]
		public Image<byte> Template { get; set; }

		[AlgorithmParameter]
		[DefaultValue(CorrelationMeasure.Zncc)]
		public CorrelationMeasure CorrelationMeasure { get; set; }

		public int[] TOff { get; set; }
		public int[] TVal { get; set; }
		public int tLen { get; set; }

		public Correlazione(Image<byte> inputImage, Image<byte> template, CorrelationMeasure correlationMeasure)
			: base(inputImage) {
			this.InputImage = inputImage;
			this.Template = template;
			CorrelationMeasure = correlationMeasure;

			if (Template.Width * Template.Height > int.MaxValue / (255 * 255))
				throw new Exception("Template troppo grande per calcolo intero a 32 bit");
		}

		public Correlazione() {
			CorrelationMeasure = CorrelationMeasure.Zncc;
		}

		public override void Run() {
			Result = new Image<int>(InputImage.Width, InputImage.Height);

			int tw2 = Template.Width / 2;
			int th2 = Template.Height / 2;
			int borderW = tw2;
			int borderH = th2;
			int w = InputImage.Width;
			int h = InputImage.Height;
			int y1 = borderH;
			int y2 = h - borderH - 1;
			int x1 = borderW;
			int x2 = w - borderW - 1;

			double normT = 0, norm2T = 0;


			if (CorrelationMeasure == CorrelationMeasure.Zncc ||
				 CorrelationMeasure == CorrelationMeasure.Znssd) {
				double avg = Avg(Template);
				Image<int> te = Subtraction(Template, avg); //Sottraggo la media al Template
				OffSetI(te, th2, tw2, w); // Costruisce l'array degli offset e quello dei valori

				//Calcola la Norma del Template meno la media
				if ((normT = NormI(te)) == 0)
					return;

				if (CorrelationMeasure == CorrelationMeasure.Znssd) {
					//Calolo la Norma al quadrato di T meno la media
					if ((norm2T = NormI2(te)) == 0)
						return;
				}
			} else {
				// Costruisce l'array degli offset e quello dei valori
				OffSetB(Template, th2, tw2, w);
			}

			//Calcolo Norma al quadrato di T
			if (CorrelationMeasure == CorrelationMeasure.Ssd ||
				 CorrelationMeasure == CorrelationMeasure.Nssd) {
				if ((norm2T = NormB2(Template)) == 0)
					return;
			}

			//Calcolo Norma di T
			if (CorrelationMeasure == CorrelationMeasure.Ncc ||
				 CorrelationMeasure == CorrelationMeasure.Nssd) {
				if ((normT = NormB(Template)) == 0)
					return;
			}

			// Esegue la correlazione scartando i bordi (i bordi restano inizializzati a zero)
			int index = borderH * w + borderW;   // indice lineare all'interno dell'immagine
			int indexStepRow = borderW * 2;
			int templOriginIndex = 0; // l'angolo in alto a sinistra del template parte dal pixel 0,0
			int ImgTemplStepRow = w - Template.Width;

			for (int y = y1; y <= y2; y++, index += indexStepRow, templOriginIndex += indexStepRow) {
				for (int x = x1; x <= x2; x++, templOriginIndex++) {
					int val = 0;
					double normI = 0, avg = 0;
					switch (CorrelationMeasure) {
						case CorrelationMeasure.CC:
							val = (int)CC(index);
							break;

						case CorrelationMeasure.Ncc:
							normI = NormB(InputImage, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow);
							if (normI > 0) {
								long lval = CC(index) * 64;
								val = (int)(lval / (int)(normI * normT));
							}
							break;

						case CorrelationMeasure.Nssd:
							normI = NormB(InputImage, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow);
							if (normI > 0) {
								long lval = (long)(NormB2(InputImage, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow) + norm2T - 2 * CC(index));
								lval *= 64;
								val = (int)(lval / (normI * normT));
							} else val = 255;
							// Per semplicità i valori troppo alti sono troncati (utile nel caso di visualizzazione come immagine con 1 byte per pixel)
							if (val > 255)
								val = 255;
							break;

						case CorrelationMeasure.Ssd:
							val = (int)(NormB2(InputImage, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow) + norm2T - 2 * CC(index));
							break;

						case CorrelationMeasure.Zncc:
							avg = Avg(InputImage, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow);
							normI = NormBAvg(InputImage, avg, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow);
							if (normI > 0) {
								long lval = CCAvg(index, avg) * 64;
								val = (int)(lval / (int)(normI * normT));
							}
							break;

						case CorrelationMeasure.Znssd:
							avg = Avg(InputImage, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow);
							normI = NormBAvg(InputImage, avg, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow);
							if (normI > 0) {
								long lval = (long)(NormB2Avg(InputImage, avg, templOriginIndex, Template.Width, Template.Height, ImgTemplStepRow) + norm2T - 2 * CCAvg(index, avg));
								lval *= 64;
								val = (int)(lval / (normI * normT));
							} else val = 255;
							// Per semplicità i valori troppo alti sono troncati (utile nel caso di visualizzazione come immagine con 1 byte per pixel)
							if (val > 255)
								val = 255;

							break;
					}
					Result[index++] = val;
				}
			}
		}

		//Calcola la Cross Correlation di una Sottoregione di Img con il template T
		private long CC(int index) {
			long val = 0;

			for (int k = 0; k < tLen; k++)
				val += (int)InputImage[index + TOff[k]] * TVal[k];

			return val;
		}

		//Calcola la Cross Correlation di una Sottoregione di Img meno la media con il template T
		private long CCAvg(int index, double avg) {
			long val = 0;

			for (int k = 0; k < tLen; k++)
				val += (int)(InputImage[index + TOff[k]] - avg) * TVal[k];

			return val;
		}

		//Calcola la Norma una Sottoregione di una Image<byte>
		private double NormB(Image<byte> img, int index, int w, int h, int stepRow) {
			return Math.Sqrt(NormB2(img, index, w, h, stepRow));
		}

		//Calcolo gli offset e i valori diversi da 0 del Template(Image<byte>)
		private void OffSetB(Image<byte> te, int mh2, int mw2, int w) {
			int nM = te.Width * te.Height;
			TOff = new int[nM];
			TVal = new int[nM];
			tLen = 0;
			for (int y = 0, i = 0; y < te.Height; y++)
				for (int x = 0; x < te.Width; x++, i++) {
					int t = te[i];
					if (t != 0) {
						TOff[tLen] = (y - mh2) * w + (x - mw2); // segno +
						TVal[tLen] = t;
						tLen++;
					}
				}
		}

		//Calcolo gli offset e i valori diversi da 0 del Template(Image<int>)
		private void OffSetI(Image<int> te, int mh2, int mw2, int w) {
			int nM = te.Width * te.Height;
			TOff = new int[nM];
			TVal = new int[nM];
			tLen = 0;
			for (int y = 0, i = 0; y < te.Height; y++)
				for (int x = 0; x < te.Width; x++, i++) {
					int t = te[i];
					if (t != 0) {
						TOff[tLen] = (y - mh2) * w + (x - mw2); // segno +
						TVal[tLen] = t;
						tLen++;
					}
				}
		}

		//Calcola la Norma al quadrato di una VAGrayImageInt
		private double NormI2(Image<int> img) {
			double norm2 = 0;

			foreach (int p in img)
				norm2 += (double)p * p;

			return norm2;
		}

		//Calcola la Norma al quadrato di una VAGrayImageInt
		private double NormI(Image<int> img) {
			return Math.Sqrt(NormI2(img));
		}

		//Calcola la Norma al quadrato di una Image<byte>
		private double NormB2(Image<byte> img) {
			double norm2 = 0;

			foreach (byte b in img)
				norm2 += (double)b * b;

			return norm2;
		}

		//Calcola la Norma al quadrato di una Sottoregione di una Image<byte>
		private double NormB2(Image<byte> img, int index, int w, int h, int stepRow) {
			double norm2 = 0;

			for (int y = 0; y < h; y++, index += stepRow) {
				for (int x = 0; x < w; x++) {
					double p = img[index++];
					norm2 += p * p;
				}
			}

			return norm2;
		}

		//Calcola la Norma di una Image<byte>
		private double NormB(Image<byte> img) {
			return Math.Sqrt(NormB2(img));
		}

		//Calcola la Norma al quadrato di una Sottoregione di una VAGrayImageByte meno la sua media
		private double NormB2Avg(Image<byte> img, double avg, int index, int w, int h, int stepRow) {
			double norm2 = 0;

			for (int y = 0; y < h; y++, index += stepRow) {
				for (int x = 0; x < w; x++) {
					double p = img[index++] - avg;
					norm2 += p * p;
				}
			}

			return norm2;
		}

		//Calcola la Norma di una Sottoregione di una VAGrayImageByte meno la sua media
		private double NormBAvg(Image<byte> img, double avg, int index, int w, int h, int stepRow) {
			return Math.Sqrt(NormB2Avg(img, avg, index, w, h, stepRow));
		}

		//Sottrae ad ogni pixel di una Image<byte> il valore s e restituisce il risultato in una Image<int>
		private Image<int> Subtraction(Image<byte> img, double s) {
			Image<int> res = new Image<int>(img.Width, img.Height);


			for (int i = 0; i < img.PixelCount; i++)
				res[i] = (int)(img[i] - s);

			return res;
		}

		//Calcola la media di una Image<byte> 
		private double Avg(Image<byte> img) {
			double avg = 0;

			foreach (byte b in img)
				avg += b;

			avg /= img.PixelCount;
			return avg;
		}

		//Calcola la media di una Sottoregione di una Image<byte> 
		private double Avg(Image<byte> img, int index, int w, int h, int stepRow) {
			double avg = 0;
			int count = 0;

			for (int y = 0; y < h; y++, index += stepRow) {
				for (int x = 0; x < w; x++) {
					avg += img[index++];
					count++;
				}
			}

			avg /= count;

			return avg;
		}
	}
}
