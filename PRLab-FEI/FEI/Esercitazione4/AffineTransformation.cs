﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	public abstract class TrasformazioneAffine<TImage> : ImageOperation<TImage, TImage>
	  where TImage : ImageBase {
		protected TrasformazioneAffine() {
		}

		protected TrasformazioneAffine(TImage inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, int resultWidth, int resultHeight)
			: base(inputImage) {
			TranslationX = translationX;
			TranslationY = translationY;
			RotationDegrees = rotationDegrees;
			ScaleFactorX = scaleFactorX;
			ScaleFactorY = scaleFactorY;
			ResultWidth = resultWidth;
			ResultHeight = resultHeight;
		}

		protected TrasformazioneAffine(TImage inputImage, double translationX, double translationY, double rotationDegrees)
			: this(inputImage, translationX, translationY, rotationDegrees, 1, 1, inputImage.Width, inputImage.Height) {
		}

		protected TrasformazioneAffine(TImage inputImage, double scaleFactor)
			: this(inputImage, 0, 0, 0, scaleFactor, scaleFactor, 0, 0) {
		}

		private double translationX;
		private double rotationDegrees;
		private double translationY;
		private double cx = 0.5;
		private double cy = 0.5;
		private double scaleFactorX = 1.0;
		private double scaleFactorY = 1.0;

		[AlgorithmParameter]
		[DefaultValue(0)]
		public double TranslationX { get { return translationX; } set { translationX = value; } }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public double TranslationY { get { return translationY; } set { translationY = value; } }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public double RotationDegrees { get { return rotationDegrees; } set { rotationDegrees = value; } }

		[AlgorithmParameter]
		[DefaultValue(0.5)]
		public double RotationCenterX { get { return cx; } set { cx = value; } }

		[AlgorithmParameter]
		[DefaultValue(0.5)]
		public double RotationCenterY { get { return cy; } set { cy = value; } }

		[AlgorithmParameter]
		[DefaultValue(1)]
		public double ScaleFactorX { get { return scaleFactorX; } set { scaleFactorX = value; } }

		[AlgorithmParameter]
		[DefaultValue(1)]
		public double ScaleFactorY { get { return scaleFactorY; } set { scaleFactorY = value; } }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int ResultWidth { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int ResultHeight { get; set; }
	}

	[AlgorithmInfo("Trasformazione affine (grayscale)", Category = "FEI")]
	public class TrasformazioneAffineGrayscale : TrasformazioneAffine<Image<byte>> {

		protected double SinMinusTheta,
						  CosMinusTheta,
						  ScaleX,
						  ScaleY,
						  RotationCenter_X,
						  RotationCenter_Y,
						  Translation_X,
						  Translation_Y,
						  wA,
						  wB,
						  wC,
						  wD;

		protected int x1,
							 x2,
							 y1,
							 y2;

		[AlgorithmParameter]
		[DefaultValue(0)]
		public byte Background { get; set; }

		public TrasformazioneAffineGrayscale(Image<byte> inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, byte background, int resultWidth, int resultHeight)
			: base(inputImage, translationX, translationY, rotationDegrees, scaleFactorX, scaleFactorY, resultWidth, resultHeight) {
			Background = background;
		}

		public TrasformazioneAffineGrayscale(Image<byte> inputImage, double translationX, double translationY, double rotationDegrees, byte background)
			: base(inputImage, translationX, translationY, rotationDegrees) {
			Background = background;
		}

		public TrasformazioneAffineGrayscale(Image<byte> inputImage, double scaleFactor, byte background)
			: base(inputImage, scaleFactor) {
			Background = background;
		}

		public TrasformazioneAffineGrayscale() {
		}

		protected void PreCalculateParams() {
			double thetaRad = -(Math.PI * this.RotationDegrees) / 180;
			SinMinusTheta = Math.Sin(thetaRad);
			CosMinusTheta = Math.Cos(thetaRad);
			ScaleX = 1 / this.ScaleFactorX;
			ScaleY = 1 / this.ScaleFactorY;
			RotationCenter_X = this.RotationCenterX * InputImage.Width;
			RotationCenter_Y = this.RotationCenterY * InputImage.Height;
			Translation_X = this.TranslationX + RotationCenter_X;
			Translation_Y = this.TranslationY + RotationCenter_Y;
		}

		//Funzione di mapping inverso
		protected void InvMapping(int xN, int yN) {
			double a = xN - Translation_X,
					  b = yN - Translation_Y,
					  xO = ScaleX * ((CosMinusTheta * a) + (SinMinusTheta * b)) + RotationCenter_X,
					  yO = ScaleY * ((-SinMinusTheta * a) + (CosMinusTheta * b)) + RotationCenter_Y;


			x1 = (int)Math.Floor(xO);
			x2 = x1 + 1;
			y1 = (int)Math.Floor(yO);
			y2 = y1 + 1;
			wA = (x2 - xO) * (y2 - yO);
			wB = (xO - x1) * (y2 - yO);
			wC = (xO - x1) * (yO - y1);
			wD = (x2 - xO) * (yO - y1);
		}

		//Interpolazione di Lagrange
		protected byte Interpolate(byte iA, byte iB, byte iC, byte iD) {
			double val = iA * wA + iB * wB + iC * wC + iD * wD;
			return val.RoundAndClipToByte();
		}

		//Controllo se il pixel cade nell'immagine di partenza o meno
		private byte ImgOrBack(int x, int y) {
			if (x >= 0 && x < InputImage.Width && y >= 0 && y < InputImage.Height)
				return ((Image<byte>)InputImage)[y, x];
			else return Background;
		}

		public override void Run() {
			//Creo l'immagine di input
			Result = new Image<byte>(InputImage.Width, InputImage.Height, Background);

			//Calcolo i parametri
			PreCalculateParams();

			int i = 0;
			for (int y = 0; y < Result.Height; y++) {
				for (int x = 0; x < Result.Width; x++) {
					InvMapping(x, y);
					Result[i++] = Interpolate(ImgOrBack(x1, y1),
													ImgOrBack(x2, y1),
													ImgOrBack(x2, y2),
													ImgOrBack(x1, y2));
				}
			}
		}
	}

	[AlgorithmInfo("Trasformazione affine (rgb)", Category = "FEI")]
	public class TrasformazioneAffineRgb : TrasformazioneAffine<RgbImage<byte>> {

		protected double SinMinusTheta,
				  CosMinusTheta,
				  ScaleX,
				  ScaleY,
				  RotationCenter_X,
				  RotationCenter_Y,
				  Translation_X,
				  Translation_Y,
				  wA,
				  wB,
				  wC,
				  wD;

		protected int x1,
							 x2,
							 y1,
							 y2;

		[AlgorithmParameter]
		public RgbPixel<byte> Background { get; set; }

		public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, RgbPixel<byte> background, int resultWidth, int resultHeight)
			: base(inputImage, translationX, translationY, rotationDegrees, scaleFactorX, scaleFactorY, resultWidth, resultHeight) {
			Background = background;
		}

		public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double translationX, double translationY, double rotationDegrees, RgbPixel<byte> background)
			: base(inputImage, translationX, translationY, rotationDegrees) {
			Background = background;
		}

		public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double scaleFactor, RgbPixel<byte> background)
			: base(inputImage, scaleFactor) {
			Background = background;
		}

		public TrasformazioneAffineRgb() {
		}

		protected void PreCalculateParams() {
			double thetaRad = -(Math.PI * this.RotationDegrees) / 180;
			SinMinusTheta = Math.Sin(thetaRad);
			CosMinusTheta = Math.Cos(thetaRad);
			ScaleX = 1 / this.ScaleFactorX;
			ScaleY = 1 / this.ScaleFactorY;
			RotationCenter_X = this.RotationCenterX * InputImage.Width;
			RotationCenter_Y = this.RotationCenterY * InputImage.Height;
			Translation_X = this.TranslationX + RotationCenter_X;
			Translation_Y = this.TranslationY + RotationCenter_Y;
		}

		//Funzione di mapping inverso
		protected void InvMapping(int xN, int yN) {
			double a = xN - Translation_X,
					  b = yN - Translation_Y,
					  xO = ScaleX * ((CosMinusTheta * a) + (SinMinusTheta * b)) + RotationCenter_X,
					  yO = ScaleY * ((-SinMinusTheta * a) + (CosMinusTheta * b)) + RotationCenter_Y;


			x1 = (int)Math.Floor(xO);
			x2 = x1 + 1;
			y1 = (int)Math.Floor(yO);
			y2 = y1 + 1;
			wA = (x2 - xO) * (y2 - yO);
			wB = (xO - x1) * (y2 - yO);
			wC = (xO - x1) * (yO - y1);
			wD = (x2 - xO) * (yO - y1);
		}

		private RgbPixel<byte> ImgOrBack(int x, int y) {
			if (x >= 0 && x < InputImage.Width && y >= 0 && y < InputImage.Height)
				return ((RgbImage<byte>)InputImage)[y, x];
			else return Background;
		}

		protected byte Interpolate(byte iA, byte iB, byte iC, byte iD) {
			double val = iA * wA + iB * wB + iC * wC + iD * wD;
			return ImageUtilities.RoundAndClipToByte(val);
		}

		public override void Run() {
			//Creo l'immagine di input
			Result = new RgbImage<byte>(InputImage.Width, InputImage.Height);

			//Calcolo i parametri
			PreCalculateParams();

			int i = 0;
			for (int y = 0; y < Result.Height; y++) {
				for (int x = 0; x < Result.Width; x++) {
					InvMapping(x, y);

					RgbPixel<byte> A = ImgOrBack(x1, y1),
										B = ImgOrBack(x2, y1),
										C = ImgOrBack(x2, y2),
										D = ImgOrBack(x1, y2);

					Result[i++] = new RgbPixel<byte>(Interpolate(A.Red, B.Red, C.Red, D.Red),
															  Interpolate(A.Green, B.Green, C.Green, D.Green),
															  Interpolate(A.Blue, B.Blue, C.Blue, D.Blue));
				}
			}
		}
	}
}
