﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI {

	[AlgorithmInfo("Negativo Rgb", Category = "FEI")]
	public class RgbNegativeImage : ImageOperation<RgbImage<byte>, RgbImage<byte>> {
		public override void Run() {

			//Creo una nuova immagine RGB in output
			Result = new RgbImage<byte>(InputImage.Width, InputImage.Height);

			//Scorro tutti gli elementi dell'immagine RGB input
			for (int i = 0; i < InputImage.PixelCount; i++) {
				//Per ogni canale vado a fare le conversioni

				//Canale BLU
				switch (InputImage.BlueChannel[i]) {
					//Se è 255 lo metto a 0
					case 255:
						Result.BlueChannel[i] = 0;
						break;

					//Se è 0 lo metto a 255
					case 0:
						Result.BlueChannel[i] = 255;
						break;

					//Altrimenti lo nego
					default:
						Result.BlueChannel[i] -= InputImage.BlueChannel[i];
						break;
				}

				//Canale VERDE
				switch (InputImage.GreenChannel[i]) {
					//Se è 255 lo metto a 0
					case 255:
						Result.GreenChannel[i] = 0;
						break;

					//Se è 0 lo metto a 255
					case 0:
						Result.GreenChannel[i] = 255;
						break;

					//Altrimenti lo nego
					default:
						Result.GreenChannel[i] -= InputImage.GreenChannel[i];
						break;
				}

				//Canale ROSSO
				switch (InputImage.RedChannel[i]) {
					//Se è 255 lo metto a 0
					case 255:
						Result.RedChannel[i] = 0;
						break;

					//Se è 0 lo metto a 255
					case 0:
						Result.RedChannel[i] = 255;
						break;

					//Altrimenti lo nego
					default:
						Result.RedChannel[i] -= InputImage.RedChannel[i];
						break;
				}
			}
		}
	}
}

