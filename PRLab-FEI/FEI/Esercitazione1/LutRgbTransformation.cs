﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {
	[AlgorithmInfo("Look Up Table Transform (RGB)", Category = "FEI")]
	public class LutRgbTransformation : ImageOperation<Image<byte>, RgbImage<byte>> {
		RgbPixel<byte>[] lut = new RgbPixel<byte>[256];

		[AlgorithmParameter]
		[DefaultValue(0)]
		public RgbPixel<byte>[] LookupTable {
			get { return lut; }
			set { lut = value; }
		}

		public override void Run() {
			var op = new RgbLookupTableTransform<byte>(InputImage, lut);
			Result = op.Execute();
		}
	}
}
