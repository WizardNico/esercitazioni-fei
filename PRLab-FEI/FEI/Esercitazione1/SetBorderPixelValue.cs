﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Settare bordi di un'immagine", Category = "FEI")]
	public class SetBorderPixelValue : ImageOperation<Image<byte>, Image<byte>> {
		[AlgorithmParameter]
		[DefaultValue(0)]
		public int BorderSize { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public byte Value { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public bool SameImg { get; set; }

		public SetBorderPixelValue(Image<byte> img, int borderSize, byte value, bool sameImg) {
			InputImage = img;
			BorderSize = borderSize;
			Value = value;
			SameImg = sameImg;
		}

		public override void Run() {
			Result = SameImg ? InputImage : (Image<byte>)InputImage.Clone();

			int index1 = 0;
			int index2 = InputImage.PixelCount - 1;
			for (int i = 0; i < BorderSize; i++)
				for (int x = 0; x < Result.Width; x++, index1++, index2--)
					Result[index1] = Result[index2] = Value;
			int h = Result.Height - BorderSize * 2;
			int w = Result.Width;
			int step = -w * h + 1;
			for (int i = 0; i < BorderSize; i++, index1 += step, index2 -= step)
				for (int y = 0; y < h; y++, index1 += w, index2 -= w)
					Result[index1] = Result[index2] = (byte)Value;
		}
	}
}


