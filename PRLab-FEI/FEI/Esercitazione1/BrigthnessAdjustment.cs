﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Regolatore di luminostià", Category = "FEI")]
	public class BrigthnessAdjustmen : ImageOperation<Image<byte>, Image<byte>> {
		private int brigthnessVaration;

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int BrigthnessVaration {
			get { return brigthnessVaration; }
			set {
				if (value < -100 || value > 100)
					MessageBox.Show("Inserire solo valori compresi tra 100 e -100", "Valore non ammesso", MessageBoxButtons.OK, MessageBoxIcon.Error);
				brigthnessVaration = value;
			}
		}

		public override void Run() {
			Result = new LookupTableTransform<byte>(InputImage, (p => (p + brigthnessVaration * 255 / 100).ClipToByte())).Execute();
		}
	}
}

