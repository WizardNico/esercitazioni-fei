﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {
	[AlgorithmInfo("Look Up Table Transform (Grayscale)", Category = "FEI")]
	public class LutGrayscaleTransformation : ImageOperation<Image<byte>, Image<byte>> {
		byte[] lut = new byte[256];

		[AlgorithmParameter]
		[DefaultValue(0)]
		public byte[] LookupTable {
			get { return lut; }
			set { lut = value; }
		}

		public override void Run() {
			var op = new LookupTableTransform<byte>(InputImage, lut);
			Result = op.Execute();
		}
	}
}
