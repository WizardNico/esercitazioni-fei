﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI.Esercitazione1 {
	[AlgorithmInfo("Trasformazione Grayscale in RGB", Category = "FEI")]
	public class GrayscaleToRgb : ImageOperation<Image<byte>, RgbImage<byte>> {


		public static RgbPixel<byte> fillPalette(float progress) {
			float div = (Math.Abs(progress % 1) * 6);
			int ascending = (int)((div % 1) * 255);
			int descending = 255 - ascending;

			switch ((int)div) {
				case 0:
					return new RgbPixel<byte>(255, ascending.ClipToByte(), 0);
				case 1:
					return new RgbPixel<byte>(descending.ClipToByte(), 255, 0);
				case 2:
					return new RgbPixel<byte>(0, 255, ascending.ClipToByte());
				case 3:
					return new RgbPixel<byte>(0, descending.ClipToByte(), 255);
				case 4:
					return new RgbPixel<byte>(ascending.ClipToByte(), 0, 255);
				default: // case 5:
					return new RgbPixel<byte>(255, 0, descending.ClipToByte());
			}
		}

		public override void Run() {
			//Creo una nuova immagine RGB in output
			Result = new RgbImage<byte>(InputImage.Width, InputImage.Height);
			var lut = new RgbPixel<byte>[256];
			int index = 0;

			for (double i = 0; i < 2.55; i += 0.01) {
				lut[index] = fillPalette((float)i);
				index++;
			}

			for (int i = 0; i < Result.PixelCount; i++)
				Result[i] = lut[InputImage[i]];
		}
	}
}
