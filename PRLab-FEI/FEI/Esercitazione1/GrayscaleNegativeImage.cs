﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI {

	[AlgorithmInfo("Negativo Grayscale", Category = "FEI")]
	public class GrayscaleNegativeImage : ImageOperation<Image<byte>, Image<byte>> {
		public override void Run() {

			//Imposto l'immagine Result come negativo dell'immagine InputImage
			Result = new Image<byte>(InputImage.Width, InputImage.Height);

			//Scorro ogni pixel dell'immagine di input
			for (int i = 0; i < InputImage.PixelCount; i++) {
				switch (InputImage[i]) {

					//Se è 255 allora lo pongo a 0 nell'immagine Result di output
					case 255:
						Result[i] = 0;
						break;

					//Se è 0 allora lo pongo a 255 nell'immagine Result di output
					case 0:
						Result[i] = 255;
						break;

					//Aaltrimenti mi basta semplicemente negarlo
					default:
						Result[i] -= InputImage[i];
						break;
				}
			}
		}
	}
}
