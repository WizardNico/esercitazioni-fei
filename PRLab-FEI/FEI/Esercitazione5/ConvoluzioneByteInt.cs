﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {

	[AlgorithmInfo("Convoluzione (da immagine byte a immagine int)", Category = "FEI")]
	[CustomAlgorithmPreviewParameterControl(typeof(SimpleConvolutionParameterControl))]
	public class ConvoluzioneByteInt : Convolution<byte, int, ConvolutionFilter<int>> {

		public ConvoluzioneByteInt(Image<byte> inputImage, ConvolutionFilter<int> filter) {
			this.Filter = filter;
			this.InputImage = inputImage;
		}

		public override void Run() {
			Result = new Image<int>(InputImage.Width, InputImage.Height);
			if (Filter.Size == 0 || Filter == null) {
				Result = InputImage.Clone().ToIntImage();
			} else {
				int w = InputImage.Width;
				int h = InputImage.Height;
				int m = Filter.Size;
				int m2 = m / 2;
				int border = m2 + AdditionalBorder;
				int x1 = border;
				int x2 = w - border;
				int y1 = border;
				int y2 = h - border;
				int nM = Filter.Size * Filter.Size;
				int[] FOff = new int[nM];
				int[] FVal = new int[nM];
				int maskLen = 0;

				for (int y = 0; y < Filter.Size; y++)
					for (int x = 0; x < Filter.Size; x++)
						if (Filter[y, x] != 0) {
							FOff[maskLen] = (m2 - y) * w + (m2 - x);
							FVal[maskLen] = Filter[y, x];
							maskLen++;
						}

				int index = border * (w + 1); // indice lineare all'interno dell'immagine
				int indexStepRow = 2 * border; // aggiustamento indice a fine riga (salta bordi)

				for (int y = y1; y < y2; y++, index += indexStepRow)
					for (int x = x1; x < x2; x++) {
						int val = 0;
						for (int k = 0; k < maskLen; k++)
							val += InputImage[index + FOff[k]] * FVal[k];
						Result[index++] = val / Filter.Denominator;
					}
			}
		}
	}
}



