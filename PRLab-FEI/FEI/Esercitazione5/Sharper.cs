﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI.Esercitazione5 {
	[AlgorithmInfo("Sharpening di un'immagine", Category = "FEI")]
	public class Sharper : ImageOperation<Image<byte>, Image<byte>> {
		private double Strength;

		[AlgorithmParameter]
		[DefaultValue(0.0)]
		public double ConvolutionStrength {
			get { return Strength; }
			set {
				if (value < 0 || value > 1)
					MessageBox.Show("Inserire un valore fra 0 and 1.", "Valore non ammesso", MessageBoxButtons.OK, MessageBoxIcon.Error);
				else
					Strength = (double)value;
			}
		}

		public override void Run() {
			ConvolutionFilter<int> filter = new ConvolutionFilter<int>(new int[] { -1, -1, -1, -1, 8, -1, -1, -1, -1 }, 1);
			Image<int> sharpImage = new ConvoluzioneByteInt(InputImage, filter).Execute();
			Result = new Image<byte>(InputImage.Width, InputImage.Height);

			for (int i = 0; i < Result.PixelCount; i++)
				Result[i] = ImageUtilities.RoundAndClipToByte((int)InputImage[i] + sharpImage[i] * Strength);

		}
	}
}
