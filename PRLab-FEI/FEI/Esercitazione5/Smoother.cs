﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI.Esercitazione5 {
	[AlgorithmInfo("Smoothing di un'immagine", Category = "FEI")]
	public class Smoother : ImageOperation<Image<byte>, Image<byte>> {

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int SmoothWindowSize { get; set; }

		public override void Run() {
			int[] filterVal = new int[SmoothWindowSize * SmoothWindowSize];

			for (int i = 0; i < filterVal.Length; i++)
				filterVal[i] = 1;

			//Creo un filtro per la convoluzione e gli metto: la grandezza del filtro, denominatore 
			ConvolutionFilter<int> filter = new ConvolutionFilter<int>(filterVal, SmoothWindowSize * SmoothWindowSize);
			Result = new ConvoluzioneByteInt(InputImage, filter).Execute().ToByteImage();
		}
	}
}
