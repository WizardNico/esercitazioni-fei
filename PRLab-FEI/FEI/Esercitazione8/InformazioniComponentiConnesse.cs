﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {
	[AlgorithmInfo("Informazioni componenti connesse", Category = "FEI")]
	public class InformazioniComponentiConnesse : Algorithm {

		[AlgorithmInput]
		public Image<byte> BinaryImage { get; set; }

		[AlgorithmParameter]
		[DefaultValue(255)]
		public byte Foreground { get; set; }

		[AlgorithmOutput]
		public Int32 ComponentCount { get; set; }

		[AlgorithmOutput]
		public Dictionary<Int32, Int32> MinAreaLabel { get; set; }

		[AlgorithmOutput]
		public Dictionary<Int32, Int32> MaxAreaLabel { get; set; }

		[AlgorithmOutput]
		public Double AvargeArea { get; set; }

		[AlgorithmOutput]
		public Double AvaragePermieter { get; set; }


		public override void Run() {
			ConnectedComponentImage connectedComponentImage = new EtichettaturaComponentiConnesse(BinaryImage, Foreground).Execute();

			//Trovo il numero di componenti connesse
			ComponentCount = connectedComponentImage.ComponentCount;

			var cursor = new ImageCursor(connectedComponentImage, 1); // per semplicità ignora i bordi (1 pixel) 
			Dictionary<Int32, Int32> areas = new Dictionary<Int32, Int32>();

			//Creo una mappa con i valori di ogni etichetta
			do {
				int l = connectedComponentImage[cursor];
				int value = 0;
				if (l >= 0) {
					if (areas.ContainsKey(l)) {
						areas.TryGetValue(l, out value);
						value++;
						areas.Remove(l);
						areas.Add(l, value);
					} else {
						areas.Add(l, 1);
					}
				}
			} while (cursor.MoveNext());

			//Calcolo l'area minima e massima
			var MinArea = Convert.ToInt32(areas.Values.ToArray().GetValue(0));
			var MaxArea = 0;
			MaxAreaLabel = new Dictionary<Int32, Int32>();
			MinAreaLabel = new Dictionary<Int32, Int32>();

			foreach (Int32 key in areas.Keys.ToList()) {
				var val = 0;
				areas.TryGetValue(key, out val);

				if (val > MaxArea) {
					MaxArea = val;
					MaxAreaLabel.Clear();
					MaxAreaLabel.Add(key, val);
				}

				if (val < MinArea) {
					MinArea = val;
					MinAreaLabel.Clear();
					MinAreaLabel.Add(key, val);
				}
			}

			//Calcolo l'area media
			AvargeArea = Convert.ToDouble(areas.Values.ToList().Sum() / areas.Values.ToList().Count());
		}
	}
}
