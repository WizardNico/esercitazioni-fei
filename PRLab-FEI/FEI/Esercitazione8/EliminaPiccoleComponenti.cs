﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {
	[AlgorithmInfo("Elimina piccole componenti connesse", Category = "FEI")]
	public class EliminaPiccoleComponenti : TopologyOperation<ConnectedComponentImage> {
		public EliminaPiccoleComponenti(Image<byte> inputImage, byte foreground, MetricType metric, Int32 minArea)
			: base(inputImage, foreground) {
			Metric = metric;
		}

		public EliminaPiccoleComponenti(Image<byte> inputImage, Int32 minArea)
			: this(inputImage, 255, MetricType.Chessboard, minArea) {
		}

		public EliminaPiccoleComponenti(Image<byte> inputImage, byte foreground, Int32 minArea)
			: this(inputImage, foreground, MetricType.Chessboard, minArea) {
		}

		public EliminaPiccoleComponenti() {
			Metric = MetricType.Chessboard;
		}

		private Dictionary<Int32, Int32> getAreas() {
			Dictionary<Int32, Int32> areas = new Dictionary<Int32, Int32>();
			var cursor = new ImageCursor(InputImage, 1); // per semplicità ignora i bordi (1 pixel) 

			//Creo una mappa con i valori di ogni etichetta
			cursor.Restart();
			do {
				int l = Result[cursor];
				int value = 0;
				if (l >= 0) {
					if (areas.ContainsKey(l)) {
						areas.TryGetValue(l, out value);
						value++;
						areas.Remove(l);
						areas.Add(l, value);
					} else {
						areas.Add(l, 1);
					}
				}
			} while (cursor.MoveNext());

			return areas;
		}

		private List<int> foundTargetLabels(Dictionary<Int32, Int32> map, out int removedLabels) {
			var targetLabels = new List<int>();
			removedLabels = 0;
			foreach (Int32 k in map.Keys) {
				var val = 0;
				map.TryGetValue(k, out val);

				if ((val < MinArea) && (!targetLabels.Contains(k))) {
					targetLabels.Add(k);
					removedLabels++;
				}
			}
			return targetLabels;
		}

		[AlgorithmParameter]
		[DefaultValue(MetricType.Chessboard)]
		public MetricType Metric { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int MinArea { get; set; }

		public override void Run() {
			Result = new ConnectedComponentImage(InputImage.Width, InputImage.Height, -1);

			var cursor = new ImageCursor(InputImage, 1); // per semplicità ignora i bordi (1 pixel) 

			int[] neighborLabels = new int[Metric == MetricType.CityBlock ? 2 : 4];
			int nextLabel = 0;
			var equivalences = new DisjointSets(InputImage.PixelCount);

			do { // prima scansione
				if (InputImage[cursor] == Foreground) {
					int labelCount = 0;
					if (Result[cursor.West] >= 0) neighborLabels[labelCount++] = Result[cursor.West];
					if (Result[cursor.North] >= 0) neighborLabels[labelCount++] = Result[cursor.North];
					if (Metric == MetricType.Chessboard) {   // anche le diagonali
						if (Result[cursor.Northwest] >= 0) neighborLabels[labelCount++] = Result[cursor.Northwest];
						if (Result[cursor.Northeast] >= 0) neighborLabels[labelCount++] = Result[cursor.Northeast];
					}
					if (labelCount == 0) {
						equivalences.MakeSet(nextLabel); // crea un nuovo set
						Result[cursor] = nextLabel++; // le etichette iniziano da 0
					} else {
						int l = Result[cursor] = neighborLabels[0]; // seleziona la prima
						for (int i = 1; i < labelCount; i++) // equivalenze
							if (neighborLabels[i] != l)
								equivalences.MakeUnion(neighborLabels[i], l); // le rende equivalenti
					}
				}
			} while (cursor.MoveNext());

			// rende le etichette numeri consecutivi 
			int totalLabels;
			int[] corresp = equivalences.Renumber(nextLabel, out totalLabels);

			// seconda scansione per unificare le etichette
			cursor.Restart();
			do {
				int l = Result[cursor];
				if (l >= 0)
					Result[cursor] = corresp[l];
			} while (cursor.MoveNext());

			//Vado a trovare ora le etichette da rimuovere
			int removedLabels = 0;
			var labelList = foundTargetLabels(getAreas(), out removedLabels);

			// terza e ultima scansione 
			cursor.Restart();
			do {
				int l = Result[cursor];

				if (labelList.Contains(l))
					Result[cursor] = -1;

			} while (cursor.MoveNext());

			Result.ComponentCount = totalLabels;
		}
	}
}
