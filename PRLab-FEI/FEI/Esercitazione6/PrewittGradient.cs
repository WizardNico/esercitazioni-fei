﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {
	[AlgorithmInfo("Calcolo Modulo Gradiente (Prewitt)", Category = "FEI")]
	public class PrewittGradient : ImageOperation<Image<byte>, Image<int>> {

		public PrewittGradient(Image<byte> inputImage) {
			this.InputImage = inputImage;
		}

		public override void Run() {
			int[] filterValueX = new int[] { 1, 0, -1, 1, 0, -1, 1, 0, -1 };
			int[] filterValueY = new int[] { 1, 1, 1, 0, 0, 0, -1, -1, -1 };

			ConvolutionFilter<int> filterX = new ConvolutionFilter<int>(filterValueX, 3);
			ConvolutionFilter<int> filterY = new ConvolutionFilter<int>(filterValueY, 3);

			Image<int> partialDerivativeX = new ConvoluzioneByteInt(InputImage, filterX).Execute();
			Image<int> partialDerivativeY = new ConvoluzioneByteInt(InputImage, filterY).Execute();

			Result = new Image<int>(InputImage.Width, InputImage.Height);

			for (int i = 0; i < InputImage.PixelCount; i++)
				Result[i] = (int)Math.Round(Math.Sqrt(partialDerivativeX[i] * partialDerivativeX[i] + partialDerivativeY[i] * partialDerivativeY[i]));
		}
	}
}
