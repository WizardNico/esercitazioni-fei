﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {
	[AlgorithmInfo("Trasformata distanza", Category = "FEI")]
	public class TrasformataDistanza : TopologyOperation<Image<int>> {
		public TrasformataDistanza() {
		}

		public TrasformataDistanza(Image<byte> inputImage, byte foreground, MetricType metric)
			: base(inputImage, foreground) {
			Metric = metric;
		}

		public override void Run() {
			Result = new Image<int>(InputImage.Width, InputImage.Height);
			var r = Result;
			var cursor = new ImageCursor(r, 1);
			if (Metric == MetricType.CityBlock) {
				// Scansione diretta 
				do {
					if (InputImage[cursor] == Foreground) {
						r[cursor] = Math.Min(r[cursor.West], r[cursor.North]) + 1;
					}
				} while (cursor.MoveNext());
				// Scansione inversa 
				do {
					if (InputImage[cursor] == Foreground) {
						r[cursor] = Math.Min(r[cursor.East] + 1, Math.Min(r[cursor.South] + 1, r[cursor]));
					}
				} while (cursor.MovePrevious());
			} else {
				// Scansione diretta 
				do {
					if (InputImage[cursor] == Foreground) {
						r[cursor] = Math.Min(r[cursor.West], Math.Min(r[cursor.Northwest], Math.Min(r[cursor.North], r[cursor.Northeast]))) + 1;
					}
				} while (cursor.MoveNext());
				// Scansione inversa 
				do {
					if (InputImage[cursor] == Foreground) {
						r[cursor] = Math.Min(r[cursor.East] + 1, Math.Min(r[cursor.Southeast] + 1, Math.Min(r[cursor.South] + 1, Math.Min(r[cursor.Southwest] + 1, r[cursor]))));
					}
				} while (cursor.MovePrevious());
			}
		}

		[AlgorithmParameter]
		public MetricType Metric { get; set; }
	}
}
