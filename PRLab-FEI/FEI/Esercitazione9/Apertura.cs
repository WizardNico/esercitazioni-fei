﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI.Esercitazione9 {
	[AlgorithmInfo("Morfologia: Apertura", Category = "FEI")]
	public class Apertura : MorphologyOperation {
		public Apertura(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
			: base(inputImage, structuringElement, foreground) {
		}

		public Apertura(Image<byte> inputImage, Image<byte> structuringElement)
			: base(inputImage, structuringElement) {
		}

		public Apertura() {
		}

		public override void Run() {
			var erosion = new Erosione(InputImage, StructuringElement, Foreground);
			var dilation = new Dilatazione(erosion.Execute(), StructuringElement, Foreground);
			Result = dilation.Execute();
		}
	}
}
