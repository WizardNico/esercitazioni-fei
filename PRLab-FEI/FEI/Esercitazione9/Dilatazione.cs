﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {

	[AlgorithmInfo("Morfologia: Dilatazione", Category = "FEI")]
	public class Dilatazione : MorphologyOperation {
		public Dilatazione(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
			: base(inputImage, structuringElement, foreground) {
		}

		public Dilatazione(Image<byte> inputImage, Image<byte> structuringElement)
			: base(inputImage, structuringElement) {
		}

		public Dilatazione() {
		}

		public override void Run() {
			// Costruisce l'array degli offset dell'elemento strutturante riflesso
			int[] elementOffsets = MorphologyStructuringElement.CreateOffsets(StructuringElement, InputImage, true);

			Result = new Image<byte>(InputImage.Width, InputImage.Height);

			// Crea un cursore per scorrere l'immagine escludendo i pixel di bordo
			var pixelCursor = new ImageCursor(
			StructuringElement.Width / 2,
			StructuringElement.Height / 2,
			InputImage.Width - 1 - StructuringElement.Width / 2,
			InputImage.Height - 1 - StructuringElement.Height / 2,
			InputImage);

			do {
				foreach (int offset in elementOffsets) {
					if (InputImage[pixelCursor + offset] == Foreground) {
						Result[pixelCursor] = Foreground;
						break; // esce dal foreach
					}
				}
			} while (pixelCursor.MoveNext());
		}
	}
}
