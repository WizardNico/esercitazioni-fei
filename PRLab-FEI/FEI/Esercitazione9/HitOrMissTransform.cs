﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI.Esercitazione9 {
	[AlgorithmInfo("Morfologia: Hit-Or-Miss Transform", Category = "FEI")]
	public class HitOrMissTransform : MorphologyOperation {

		[AlgorithmParameter]
		public Image<byte> structuringElementBackground { get; set; }
		private Image<byte> structuringElementForeground;

		public HitOrMissTransform(Image<byte> inputImage, Image<byte> structuringElementFor, Image<byte> structuringElementBack, byte foreground)
			: base(inputImage, structuringElementFor, foreground) {
			this.structuringElementBackground = structuringElementBack;
			this.structuringElementForeground = structuringElementFor;
		}

		public HitOrMissTransform() {
		}

		public override void Run() {
			var erosionF = new Erosione(InputImage, structuringElementForeground, Foreground);
			var negative = new NegativeImage(InputImage).Execute();
			var erosionB = new Erosione(negative, structuringElementBackground, Foreground);
			Result = new Intersezione(erosionF.Execute(), erosionB.Execute(), Foreground).Execute();
		}
	}
}
