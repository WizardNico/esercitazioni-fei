﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI.Esercitazione9 {
	[AlgorithmInfo("Morfologia: Estrazione Contorno", Category = "FEI")]
	public class MorfologyContourExtractor : MorphologyOperation {
		public MorfologyContourExtractor(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
			: base(inputImage, structuringElement, foreground) {
		}

		public MorfologyContourExtractor(Image<byte> inputImage, Image<byte> structuringElement)
			: base(inputImage, structuringElement) {
		}

		public MorfologyContourExtractor() {
		}

		public override void Run() {
			var erosion = new Erosione(InputImage, StructuringElement, Foreground);
			Result = new ImageArithmetic(InputImage, erosion.Execute(), ImageArithmeticOperation.Difference).Execute();
		}
	}
}
