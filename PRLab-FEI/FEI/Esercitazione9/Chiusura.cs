﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI.Esercitazione9 {
	[AlgorithmInfo("Morfologia: Chiusura", Category = "FEI")]
	public class Chiusura : MorphologyOperation {
		public Chiusura(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
			: base(inputImage, structuringElement, foreground) {
		}

		public Chiusura(Image<byte> inputImage, Image<byte> structuringElement)
			: base(inputImage, structuringElement) {
		}

		public Chiusura() {
		}

		public override void Run() {
			var dilation = new Dilatazione(InputImage, StructuringElement, Foreground);
			var erosion = new Erosione(dilation.Execute(), StructuringElement, Foreground);
			Result = erosion.Execute();
		}
	}
}
