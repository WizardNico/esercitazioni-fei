﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {
	[AlgorithmInfo("Thinning", Category = "FEI")]
	public class Thinning : TopologyOperation<Image<byte>> {

		public override void Run() {

			byte Background = (byte)(255 - Foreground);

			// Immagine all'iterazione corrente (elimina 2 pixel di bordo)
			Image<byte> I = (Image<byte>)new SetBorderPixelValue(InputImage, 2, Background, false).Execute();

			// Immagine da utilizzare alla iterazione successiva
			Image<byte> I1 = new Image<byte>(InputImage.Width, InputImage.Height);

			ImageCursor p1 = new ImageCursor(InputImage, 1); // 1 pixel di bordo
			ImageCursor p2 = new ImageCursor(InputImage, 2); // 2 pixel di bordo
			Image<byte> A = new Image<byte>(InputImage.Width, InputImage.Height);
			Image<byte> B = new Image<byte>(InputImage.Width, InputImage.Height);

			int NumPixRemoved;
			do {
				// Fa una copia del risultato intermedio
				Image<byte> tmpCopy = (Image<byte>)I.Clone();

				NumPixRemoved = 0;

				// Calcola A(p1) e B(p1) per ogni p1
				p1.Restart();
				do {
					byte a = 0, b = 0;
					ChessboardDirection direction = ChessboardDirection.East;
					byte prev = I[p1.GetAt(ChessboardDirection.Southeast)];
					for (sbyte d = 0; d < 8; d++) {
						byte next = I[p1.GetAt(direction)];
						if (next == Foreground) {
							b++;
							if (prev != Foreground)
								a++;
						}
						prev = next;
						direction = ChessboardMetric.GetNextDirection(direction);
					}
					A[p1] = a;
					B[p1] = b;
				} while (p1.MoveNext());

				// utilizza p2 per scandire l'immagine lasciando 2 pixel di bordo
				p2.Restart();
				do {
					byte pixelValue = I[p2];
					if (pixelValue == Foreground) {
						// Se soddisfa tutte le 4 condizioni, va cancellato

						if ((2 <= B[p2] && B[p2] <= 6) && // 1
							  (A[p2] == 1)) // 2
                        {
							byte n0 = I[p2.East];
							byte n2 = I[p2.North];
							byte n4 = I[p2.West];
							byte n6 = I[p2.South];
							if ((A[p2.North] != 1 || (n2 == Background || n0 == Background || n4 == Background)) && // 3
								 (A[p2.East] != 1 || (n2 == Background || n0 == Background || n6 == Background))) // 4

                            {
								pixelValue = Background;    // lo cancella
								NumPixRemoved++;
							}
						}
					}
					I1[p2] = pixelValue;
				} while (p2.MoveNext());

				// scambia i riferimenti alle due immagini di lavoro
				Image<byte> tmp = I1;
				I1 = I;
				I = tmp;
			} while (NumPixRemoved > 0);

			Result = I1;
		}
	}
}
