﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI {
	[AlgorithmInfo("Topologia: Intersezione", Category = "FEI")]
	public class Intersezione : TopologyOperation<Image<byte>> {

		[AlgorithmInput]
		public Image<byte> InputImage2 { get; set; }

		public Intersezione(Image<byte> imgA, Image<byte> imgB, byte foreground)
			: base(imgA, foreground) {
			InputImage2 = imgB;
			if (InputImage2.Width != InputImage.Width || InputImage2.Height != InputImage.Height)
				throw new Exception("Le immagini non hanno la stessa dimensione");
		}

		public Intersezione() { }

		public override void Run() {
			byte background = (byte)(255 - Foreground);
			Result = InputImage.Clone();
			for (int i = 0; i < InputImage.PixelCount; i++)
				if (InputImage2[i] != Foreground)
					Result[i] = background;
		}
	}
}
