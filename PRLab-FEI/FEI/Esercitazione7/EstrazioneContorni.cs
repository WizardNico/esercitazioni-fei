using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{
    [AlgorithmInfo("Estrazione Contorni", Category = "FEI")]
    [BioLab.GUI.Forms.CustomAlgorithmPreviewOutput(typeof(BioLab.GUI.Forms.ContourExtractionViewer))]
    public class EstrazioneContorni : TopologyOperation<List<CityBlockContour>>
    {

        public override void Run()
        {
            // Utilizza un'array per tenere traccia dei pixel di bordo da visitare
            bool[] toBeVisited = new bool[InputImage.PixelCount];

            var ImgWork = new ImageBorderFilling<byte>(InputImage, 1, (byte)(255 - Foreground), false).Execute();

            ImageCursor p = new ImageCursor(ImgWork, 1);
            do
            {
                // Utilizza metrica d4 e cerca pixel di bordo che non siano isolati
                if (ImgWork[p] == Foreground &&
                     (ImgWork[p.North] != Foreground || ImgWork[p.East] != Foreground ||
                      ImgWork[p.West] != Foreground || ImgWork[p.South] != Foreground) &&
                     (ImgWork[p.North] == Foreground || ImgWork[p.East] == Foreground ||
                      ImgWork[p.West] == Foreground || ImgWork[p.South] == Foreground))
                    toBeVisited[p] = true;
            } while (p.MoveNext());

            // Risultato: immagine e contorni
            Result = new List<CityBlockContour>();

            // Scorre tutti i pixel da visitare
            p.Restart();
            do
            {
                if (toBeVisited[p])
                {
                    CityBlockContour c = new CityBlockContour(p.X, p.Y);
                    FollowContour(ImgWork, p, c, toBeVisited);
                    Result.Add(c);
                }
            } while (p.MoveNext());
        }

        public void FollowContour(Image<byte> I, ImageCursor pStart, CityBlockContour c, bool[] toBeVisited)
        {
            ImageCursor p = new ImageCursor(pStart);
            // Ricava la direzione di "entrata", cercando un pixel di background vicino
            CityBlockDirection direction = CityBlockDirection.West;

            do
            {
                toBeVisited[p] = false;
                for (sbyte j = 1; j <= 4; j++)
                {
                    direction = CityBlockMetric.GetNextDirection(direction);
                    if (I[p.GetAt(direction)] == Foreground)
                    {
                        break;
                    }
                }
                c.Add(direction); // nuova direzione aggiunta al contorno
                p.MoveTo(direction); // si sposta seguendo la direzione
                direction = CityBlockMetric.GetOppositeDirection(direction); // direzione rispetto al pixel precedente
            } while (p != pStart);    // si ferma quando incontra il pixel iniziale
        }
    }
}