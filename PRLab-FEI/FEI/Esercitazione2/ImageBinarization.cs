﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {
	[AlgorithmInfo("Binarizzazione", Category = "FEI")]
	public class ImageBinarization : ImageOperation<Image<byte>, Image<byte>> {

		[AlgorithmParameter]
		[DefaultValue(0)]
		public byte ThreadShould { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int SmoothWindowSize { get; set; }

		[AlgorithmParameter]
		[DefaultValue(false)]
		public bool Sqrt { get; set; }

		public ImageBinarization(Image<byte> inputImage, bool sqrt, int smoothWindowSize, byte ThreadShould) {
			InputImage = inputImage;
			Sqrt = sqrt;
			SmoothWindowSize = smoothWindowSize;
		}

		public override void Run() {
			var op = new LookupTableTransform<byte>(InputImage, p => (byte)(p < ThreadShould ? 0 : 255));
			Result = op.Execute();
		}
	}
}
