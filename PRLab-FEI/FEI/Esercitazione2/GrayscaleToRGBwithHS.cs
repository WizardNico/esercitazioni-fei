﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Colorare un'immagine grayscale (H e S)", Category = "FEI")]
	public class GrayscaleToRGBwithHS : ImageOperation<Image<byte>, RgbImage<byte>> {
		private int Hue;
		private int Saturation;

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int HueVaration {
			get { return Hue; }
			set {
				if (value < 0 || value > 255)
					throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 255.");
				else
					Hue = value;
			}
		}

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int SaturationVaration {
			get { return Saturation; }
			set {
				if (value < 0 || value > 255)
					throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 255.");
				else
					Saturation = value;
			}
		}

		public override void Run() {
			HslImage<byte> image = new HslImage<byte>(InputImage.Width, InputImage.Height);

			for (int i = 0; i < InputImage.PixelCount; i++) {
				var content = InputImage[i];
				image.SaturationChannel[i] = this.SaturationVaration.ClipToByte();
				image.HueChannel[i] = Math.Abs((this.HueVaration - content)).ClipToByte();
				image.LightnessChannel[i] = 150;
			}

			Result = image.ToByteRgbImage();
		}
	}
}
