﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Splitta immagine RGB nei tre canali HSL", Category = "FEI")]
	public class RGBtoHSL : ImageOperation<RgbImage<byte>, Image<byte>> {
		[AlgorithmOutput]
		public Image<byte> Hue { get; set; }

		[AlgorithmOutput]
		public Image<byte> Saturation { get; set; }

		[AlgorithmOutput]
		public Image<byte> Lightness { get; set; }

		public override void Run() {
			HslImage<byte> newImage = InputImage.ToByteHslImage();
			Hue = newImage.HueChannel;
			Saturation = newImage.SaturationChannel;
			Lightness = newImage.LightnessChannel;
		}
	}
}