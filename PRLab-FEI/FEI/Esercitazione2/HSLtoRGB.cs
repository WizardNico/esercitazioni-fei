﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Converte i tre canali HSL in un'immagine RGB", Category = "FEI")]
	public class HSLtoRGB : ImageOperation<Image<byte>, RgbImage<byte>> {
		[AlgorithmInput]
		public Image<byte> Hue { get; set; }

		[AlgorithmInput]
		public Image<byte> Saturation { get; set; }

		[AlgorithmInput]
		public Image<byte> Lightness { get; set; }

		public override void Run() {
			HslImage<byte> hslImage = new HslImage<byte>(Hue, Saturation, Lightness);
			Result = new HslImage<byte>(Hue, Saturation, Lightness).ToByteRgbImage();
		}
	}
}
