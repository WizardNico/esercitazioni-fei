﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{

    [AlgorithmInfo("Regolatore luminosità (RGB)", Category = "FEI")]
    public class BrightnessAdjustmentRGB : ImageOperation<RgbImage<byte>, RgbImage<byte>>
    {
        private int BrightnessVar;

        [AlgorithmParameter]
        [DefaultValue(0)]
        public int BrightnessVaration
        {
            get { return BrightnessVar; }
            set
            {
                if (value < -100 || value > 100)
                    throw new ArgumentOutOfRangeException("Inserire un valore fra -100 and 100.");
                BrightnessVar = value;
            }
        }

        public override void Run()
        {
            //Definisco l'immagine di output
            Result = InputImage.Clone().ToByteRgbImage();

            //Definisco la funzione di mapping
            PixelMapping<byte, byte> f = p => (p + BrightnessVar * 255 / 100).ClipToByte();

            var RedChannelImage = new LookupTableTransform<byte>(InputImage.RedChannel.Clone().ToByteImage(), f).Execute();
            var GreenChannelImage = new LookupTableTransform<byte>(InputImage.GreenChannel.Clone().ToByteImage(), f).Execute();
            var BlueChannelImage = new LookupTableTransform<byte>(InputImage.BlueChannel.Clone().ToByteImage(), f).Execute();

            for (int i = 0; i < Result.PixelCount; i++)
            {
                Result.RedChannel[i] = RedChannelImage[i];
                Result.GreenChannel[i] = GreenChannelImage[i];
                Result.BlueChannel[i] = BlueChannelImage[i];
            }
        }
    }
}