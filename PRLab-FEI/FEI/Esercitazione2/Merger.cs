﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Unisce tre immaini grayscale per formarne una RGB", Category = "FEI")]
	public class Merger : ImageOperation<Image<byte>, RgbImage<byte>> {
		[AlgorithmInput]
		public Image<byte> RedChannelImage { get; set; }

		[AlgorithmInput]
		public Image<byte> GreenChannelImage { get; set; }

		[AlgorithmInput]
		public Image<byte> BlueChannelImage { get; set; }

		public override void Run() {
			Result = new RgbImage<byte>(InputImage.Width, InputImage.Height);

			for (int i = 0; i < InputImage.PixelCount; i++) {
				Result.RedChannel[i] = RedChannelImage[i];
				Result.GreenChannel[i] = GreenChannelImage[i];
				Result.BlueChannel[i] = BlueChannelImage[i];
			}
		}
	}
}