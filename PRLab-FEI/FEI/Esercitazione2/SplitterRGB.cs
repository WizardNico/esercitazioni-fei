﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {

	[AlgorithmInfo("Splitta l'immagine RGB nei tre canali", Category = "FEI")]
	public class SplitterRGB : ImageOperation<RgbImage<byte>, Image<byte>> {
		[AlgorithmOutput]
		public Image<byte> RedChannelImage { get; set; }

		[AlgorithmOutput]
		public Image<byte> GreenChannelImage { get; set; }

		[AlgorithmOutput]
		public Image<byte> BlueChannelImage { get; set; }

		public override void Run() {
			RedChannelImage = new Image<byte>(InputImage.Width, InputImage.Height);
			RedChannelImage = InputImage.RedChannel;

			GreenChannelImage = new Image<byte>(InputImage.Width, InputImage.Height);
			GreenChannelImage = InputImage.GreenChannel;

			BlueChannelImage = new Image<byte>(InputImage.Width, InputImage.Height);
			BlueChannelImage = InputImage.BlueChannel;
		}
	}
}