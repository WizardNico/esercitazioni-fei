﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {
	[AlgorithmInfo("Calcolo Istogramma", Category = "FEI")]
	public class HistogramBuilder : ImageOperation<Image<byte>, Histogram> {
		private int Coordinate_Y;
		private int Coordinate_X;

		[AlgorithmParameter]
		[DefaultValue(false)]
		public bool Sqrt { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int SmoothWindowSize { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int Region_X {
			get { return Coordinate_X; }
			set {
				if (value <= InputImage.Width && value >= 0)
					Coordinate_X = value;
				else
					MessageBox.Show("Index Out Of Range", "Value not admitted", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int Region_Y {
			get { return Coordinate_Y; }
			set {
				if (value <= InputImage.Height && value >= 0)
					Coordinate_Y = value;
				else
					MessageBox.Show("Index Out Of Range", "Value not admitted", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int Region_Width { get; set; }

		[AlgorithmParameter]
		[DefaultValue(0)]
		public int Region_Heigh { get; set; }

		public HistogramBuilder(Image<byte> inputImage) {
			this.InputImage = inputImage;
		}

		public HistogramBuilder(Image<byte> inputImage, bool sqrt, int smoothWindowSize, int RegionX, int RegionY, int RegionWidth, int RegionHeigh) {
			this.InputImage = inputImage;
			this.Sqrt = sqrt;
			this.SmoothWindowSize = smoothWindowSize;
			this.Coordinate_X = RegionX;
			this.Coordinate_Y = RegionY;
			this.Region_Width = RegionWidth;
			this.Region_Heigh = RegionHeigh;
		}

		public override void Run() {
			// Calcola l'istogramma
			Result = new Histogram();
			Image<byte> SubImage = null;

			if (Region_Heigh != 0 && Region_Width != 0) {
				//Creo una nuova immagine
				SubImage = new Image<byte>(Region_Width, Region_Heigh);
				InputImage.GetSubImage(Region_X, Region_Y, SubImage);
			}

			//Calcolo l'istogramma
			if (SubImage != null) {
				foreach (byte b in SubImage)
					Result[b]++;
			} else {
				foreach (byte b in InputImage)
					Result[b]++;
			}

			//Se è richiesta la radie quadrata
			if (Sqrt == true)
				for (int i = 0; i < 256; i++)
					Result[i] = Math.Sqrt(Result[i]).RoundAndClipToByte();

			//Se è richiesto lo smoothing
			if (SmoothWindowSize != 0) {
				//Clono l'istogramma
				var tmpH = Result.Clone();

				//Scorro tutti gli elementi
				for (int i = 0; i < 256; i++) {

					//calcolo il range della finestra su cui calcolare lo smoothig
					//il range classico è (SmoothWindowSize*2)+1 ma all'inizio non ho abbastanza posti
					//quindi adatto la maschera e vado in crescendo, fino ad arrivare alla dimensione ottimale
					int j1 = Math.Max(0, i - SmoothWindowSize);
					int j2 = Math.Min(255, i + SmoothWindowSize);

					//Calcolo lo smoothing sull'istogramma
					int n = 0;
					int sum = 0;
					for (int j = j1; j <= j2; j++, n++)
						sum += tmpH[j];
					Result[i] = sum / n;
				}
			}
		}
	}
}
