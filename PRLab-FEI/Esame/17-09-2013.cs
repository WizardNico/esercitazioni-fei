﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;
using BioLab.PatternRecognition.Localization;
using System.Diagnostics;

namespace PRLab.FEI {
	//[AlgorithmInfo("17-09-2013", Category = "FEI")]
	public class Esame13 : ImageOperation<Image<byte>, Image<byte>> {
		[AlgorithmOutput]
		public Image<byte> Res1 { get; set; }
		[AlgorithmOutput]
		public Image<byte> C { get; set; }
		[AlgorithmOutput]
		public Image<byte> Res2 { get; set; }
		[AlgorithmOutput]
		public Image<byte> Res3 { get; set; }

		public override void Run() {
			/*
			 * -------------------------------------------(1)-------------------------------------------
			 * Binarizzare InputImage utilizzando come soglia la media dei livelli di grigio dei pixel nell’immagine stessa 
			 * che sono massimi locali di luminosità (considerando semplicemente l’intorno di 8 pixel di ciascuno). 
			 * Qualora nessun pixel soddisfacesse tale proprietà, utilizzare il valore 128 come soglia.
			 */
			Res1 = new Image<byte>(InputImage.Width, InputImage.Height);
			int media = 0;
			int massimo = -1;
			int count = 0;
			for (int a = 0; a < InputImage.PixelCount; a++) {
				for (int y = 0; y < 8 && y + a < InputImage.Height; y++) {
					for (int x = 0; x < 8 && x + a < InputImage.Width; x++) {
						if (InputImage[a + y, x + a] > massimo)
							massimo = InputImage[a + y, x + a];
					}

					if (massimo != -1) {
						count++;
						media = media + massimo;
						massimo = -1;
					}
				}
			}

			byte soglia = 128;
			if (count != 0)
				soglia = (byte)(media / count);
			for (int c = 0; c < InputImage.PixelCount; c++)
				Res1[c] = (byte)(InputImage[c] > soglia ? 255 : 0);



			/*
			 * -------------------------------------------(2)-------------------------------------------
			 * Eseguire, sul risultato del passo precedente, un’operazione morfologica di apertura 
			 * con un quadrato di lato 7 pixel come elemento strutturante: sia C il risultato.
			 */
			C = new Image<byte>(InputImage.Width, InputImage.Height);
			Image<byte> quadrato = MorphologyStructuringElement.CreateSquare(7);
			MorphologyErosion erosione = new MorphologyErosion(Res1, quadrato);
			erosione.Execute();
			C = new MorphologyDilation(erosione.Result, quadrato).Execute();



			/*
			 * -------------------------------------------(3)-------------------------------------------
			 * Estrarre i bordi di C utilizzando la morfologia matematica e un cerchio di diametro 3 pixel come elemento strutturante.
			 */
			Image<byte> cerchio = MorphologyStructuringElement.CreateCircle(3);
			Image<byte> temp = new MorphologyErosion(C, cerchio).Execute();
			Res2 = new Image<byte>(C.Width, C.Height);
			for (int i = 0; i < Res2.PixelCount; i++)
				Res2[i] = (C[i] - temp[i]).ClipToByte();



			/*
			 * -------------------------------------------(4)-------------------------------------------
			 * Determinare tutti i pixel di foreground di C con distanza maggiore di 9 pixel (secondo la metrica d8) dal background (valore 0).
			 */
			Image<int> DistTransf = new DistanceTransform(C, 255, MetricType.Chessboard).Execute();
			Res3 = new Image<byte>(C.Width, C.Height);
			for (int i = 0; i < DistTransf.PixelCount; i++) {
				if (DistTransf[i] > 9)
					Res3[i] = 255;
				else
					Res3[i] = 0;
			}



			/*
			 * -------------------------------------------(5)-------------------------------------------
			 * Restituire come output un’immagine grayscale (Result) in cui i pixel di bordo determinati al passo 3 hanno luminosità pari a 255, 
			 * i pixel determinati al punto 4 hanno luminosità pari a 128 e i restanti pixel hanno luminosità pari alla metà della corrispondente 
			 * luminosità in InputImage.
			 */
			Result = new Image<byte>(InputImage.Width, InputImage.Height);
			for (int i = 0; i < Result.PixelCount; i++) {
				if (Res2[i] == 255) {
					Result[i] = 255;
				} else {
					if (Res3[i] == 255) {
						Result[i] = 128;
					} else {
						Result[i] = (InputImage[i] / 2).ClipToByte();
					}
				}
			}
		}
	}
}