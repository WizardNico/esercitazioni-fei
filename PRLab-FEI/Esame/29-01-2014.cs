﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;
using BioLab.PatternRecognition.Localization;
using System.Diagnostics;

namespace PRLab.FEI {
	[AlgorithmInfo("29-01-2014", Category = "FEI")]
	public class Esame29 : ImageOperation<Image<byte>, Image<int>> {
		[AlgorithmOutput]
		public Image<byte> C { get; set; }
		[AlgorithmOutput]
		public Image<byte> Res2 { get; set; }
		[AlgorithmOutput]
		public Image<byte> Res3 { get; set; }
		[AlgorithmOutput]
		public Image<byte> Res4 { get; set; }
		[AlgorithmOutput]
		public int RowValue { get; set; }

		public override void Run() {
			/*
			 * -------------------------------------------(1)-------------------------------------------
			 * Calcolare, per ciascuna riga dell’immagine, la somma dei valori dei pixel: 
			 * sia Ym la coordinata y della riga dell’immagine con la somma maggiore (nel caso più righe soddisfino tale condizione, 
			 * considerare quella con coordinata y minore).
			 */
			List<int> rowsSums = new List<int>();

			//Calcolo le somme delle righe
			int sum = 0;
			for (int x = 0; x < InputImage.Height; x++) {
				for (int y = 0; y < InputImage.Width; y++) {
					sum += InputImage[x, y];
				}
				rowsSums.Add(sum);
				sum = 0;
			}

			//Trovo la somma più alta e quella più bassa
			int max = 0;
			int min = rowsSums.ElementAt(0);
			foreach (int element in rowsSums) {
				max = Math.Max(max, element);
				min = Math.Min(min, element);
			}


			//Trovo se l'elemento massimo ha delle ripetizioni
			int count = 0;
			bool flag = false;
			foreach (int element in rowsSums) {
				if (max == element)
					count++;

				if (count > 1) {
					flag = true;
					break;
				}
			}

			//Se ha ripetizioni prendo il più basso se no il più alto
			if (!flag) {
				for (int i = 0; i < rowsSums.Count; i++) {
					if (rowsSums.ElementAt(i) == max) {
						RowValue = i;
						break;
					}
				}
			} else {
				for (int i = 0; i < rowsSums.Count; i++) {
					if (rowsSums.ElementAt(i) == min) {
						RowValue = i;
						break;
					}
				}
			}



			/*
			 * -------------------------------------------(2)-------------------------------------------
			 * Binarizzare InputImage utilizzando come soglia la media dei livelli di grigio dei pixel con 
			 * coordinata y maggiore o uguale a Ym.
			 */
			C = InputImage.Clone();
			int media = 0;
			int pixelNumbers = 0;
			//Calcolo la media
			for (int x = RowValue; x < InputImage.Height; x++) {
				for (int y = 0; y < InputImage.Width; y++) {
					media += InputImage[x, y];
					pixelNumbers++;
				}
			}
			media /= pixelNumbers;

			//Binarizzo
			for (int i = 0; i < InputImage.PixelCount; i++)
				C[i] = (C[i] > media ? 255 : 0).ClipToByte();



			/*
			 * -------------------------------------------(3)-------------------------------------------
			 * Eseguire, sul risultato del passo precedente, un’operazione morfologica di erosione con un cerchio di diametro 7 pixel 
			 * come elemento strutturante e considerando il foreground pari a 255: sia C il risultato.
			 */
			Res2 = new Image<byte>(InputImage.Width, InputImage.Height);
			Image<byte> circle1 = MorphologyStructuringElement.CreateCircle(7);
			Res2 = new MorphologyErosion(C, circle1, 255).Execute();



			/*
			 * -------------------------------------------(4)-------------------------------------------
			 * Estrarre i bordi di C utilizzando la morfologia matematica e un cerchio di diametro 3 pixel come elemento strutturante.
			 */
			Res3 = new Image<byte>(InputImage.Width, InputImage.Height);
			Image<byte> circle2 = MorphologyStructuringElement.CreateCircle(3);
			var temp = new MorphologyDilation(C, circle2, 255).Execute();
			for (int i = 0; i < Res3.PixelCount; i++)
				Res3[i] = (temp[i] - Res2[i]).ClipToByte();



			/*
			 * -------------------------------------------(5)-------------------------------------------
			 * Determinare tutti i pixel di background di C con distanza minore di 9 pixel (secondo la metrica d8) dal foreground (valore 255).
			 */
			Res4 = new Image<byte>(InputImage.Width, InputImage.Height);
			var distImage = new DistanceTransform(C, 0, MetricType.Chessboard).Execute();
			for (int i = 0; i < Res4.PixelCount; i++) {
				if (distImage[i] < 9)
					Res4[i] = 255;
				else
					Res4[i] = 0;
			}



			/*
			 * -------------------------------------------(6)-------------------------------------------
			 * Restituire come output un’immagine di int (Result) in cui i pixel di bordo individuati al passo 4 hanno valore pari a -1, 
			 * i pixel individuati al punto 5 hanno valore pari a 0 e i restanti pixel hanno valore pari al quadrato della corrispondente 
			 * luminosità in InputImage.
			 */
			Result = new Image<int>(InputImage.Width, InputImage.Height);
			for (int i = 0; i < Result.PixelCount; i++) {
				if (Res3[i] == 255) {
					Result[i] = -1;
				} else {
					if (Res4[i] == 255) {
						Result[i] = 0;
					} else {
						Result[i] = InputImage[i] * InputImage[i];
					}
				}
			}
		}
	}
}