﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {
	//[AlgorithmInfo("27-01-2012 Es1", Category = "FEI")]
	public class BinaM : ImageOperation<Image<byte>, Image<byte>> {

		public BinaM(Image<byte> inputImage) {
			this.InputImage = inputImage;
		}

		public BinaM() {
		}

		public override void Run() {

			Result = InputImage.Clone();

			//Calcolo la media totale
			int mediaTot = 0;
			for (int i = 0; i < InputImage.PixelCount; i++)
				mediaTot += InputImage[i];

			mediaTot /= InputImage.PixelCount;

			var cursore = new ImageCursor(InputImage);
			do {
				//Calcolo la media dell'intorno
				int mediaIntorno = 0;
				for (int x = 0; x < 5 && x + cursore.X < InputImage.Height; x++)
					for (int y = 0; y < 5 && y + cursore.Y < InputImage.Width; y++)
						mediaIntorno += InputImage[cursore.X + x, cursore.Y + y];

				mediaIntorno /= 25;

				//Prendo il minimo tra i due valori
				var media = Math.Min(mediaIntorno, mediaTot);

				//Binarizzo
				Result[cursore] = (byte)(InputImage[cursore] < media ? 0 : 255);
			} while (cursore.MoveNext());
		}
	}

	//[AlgorithmInfo("27-01-2012 Es2", Category = "FEI")]
	public class BinaEContaAree : Algorithm {
		[AlgorithmInput]
		public Image<byte> InputImage { get; set; }
		[AlgorithmParameter]
		public MetricType Metric { get; set; }
		[AlgorithmOutput]
		public int componentiConnesse { get; set; }

		public override void Run() {
			byte foreground = 255;
			var binImage = new BinaM(this.InputImage).Execute();
			var labeledImage = new ConnectedComponentsLabeling(binImage, foreground, Metric).Execute();

			int[] perimeters = new int[labeledImage.ComponentCount];
			int[] areas = new int[labeledImage.ComponentCount];
			ImageCursor cursor = new ImageCursor(labeledImage);
			do {
				if (labeledImage[cursor] != -1) {
					areas[labeledImage[cursor]]++;
					if (labeledImage[cursor.North] == -1 || labeledImage[cursor.East] == -1 || labeledImage[cursor.South] == -1 || labeledImage[cursor.West] == -1)
						perimeters[labeledImage[cursor]]++;
				}
			} while (cursor.MoveNext());

			for (int i = 0; i < labeledImage.ComponentCount; i++)
				if (perimeters[i] < Convert.ToInt32(areas[i] / 2))
					componentiConnesse++;
		}
	}
}