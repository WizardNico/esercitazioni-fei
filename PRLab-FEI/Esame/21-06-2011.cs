﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI {
	//[AlgorithmInfo("Es1 istogramma", Category = "FEI")]
	public class EqualizzazioneIstogramma : ImageOperation<Image<byte>, Image<byte>> {
		public override void Run() {
			var histogram = new HistogramBuilder(this.InputImage).Execute();
			for (int i = 1; i < 256; i++)
				histogram[i] += histogram[i - 1];

			Result = new LookupTableTransform<byte>(InputImage, p => (histogram[p] * 255 / InputImage.PixelCount).ClipToByte()).Execute();
		}
	}

	//[AlgorithmInfo("Es2 due parti", Category = "FEI")]
	public class Es2 : ImageOperation<Image<byte>, ConnectedComponentImage> {
		[AlgorithmParameter]
		public byte Foreground { get; set; }
		[AlgorithmParameter]
		public MetricType Metric { get; set; }

		public override void Run() {
			//Calcolo l'etichettatura delle componenti connesse sul background
			ConnectedComponentImage invertedImage = new ConnectedComponentsLabeling(InputImage, (255 - Foreground).ClipToByte(), Metric).Execute();

			//Ricavo le aree
			int[] areas = new int[invertedImage.ComponentCount];
			ImageCursor cursor = new ImageCursor(invertedImage);
			do {
				if (invertedImage[cursor] != -1)
					areas[invertedImage[cursor]]++;
			} while (cursor.MoveNext());

			//Trovo l'area di sfondo, la maggiore tra tutte, quindi le altre saranno i buchi
			var backgroundArea = 0;
			var backgroundIndex = 0;
			for (int i = 0; i < invertedImage.ComponentCount; i++) {
				if (areas[i] > backgroundArea) {
					backgroundArea = areas[i];
					backgroundIndex = i;
				}
			}

			//Riempio i buchi
			cursor.Restart();
			do {
				if (invertedImage[cursor] != -1 && invertedImage[cursor] != backgroundIndex)
					invertedImage[cursor] = -1;
			} while (cursor.MoveNext());

			//Calcolo l'etichettatura per l'immagine originale
			Result = new ConnectedComponentsLabeling(invertedImage.ToByteImage(), (255 - Foreground).ClipToByte(), Metric).Execute(); ;
		}
	}
}