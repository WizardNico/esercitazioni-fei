﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using BioLab.Common;
using BioLab.GUI.Forms;
using BioLab.ImageProcessing;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI {
	//[AlgorithmInfo("27-09-2011 Es1", Category = "FEI")]
	public class Binarizza : ImageOperation<Image<byte>, Image<byte>> {

		public Binarizza(Image<byte> inputImage) {
			this.InputImage = inputImage;
		}

		public Binarizza() {
		}

		public override void Run() {
			Result = InputImage.Clone();
			int width = InputImage.Width;
			int height = InputImage.Height;
			ImageCursor curs = new ImageCursor(InputImage);
			do {
				int media = 0;
				for (int x = 0; x < 5 && x + curs.X < height; x++)//intorno di 5
					for (int y = 0; y < 5 && y + curs.Y < width; y++)
						media += InputImage[curs.X + x, curs.Y + y];

				media /= 25;
				Result[curs] = (byte)(InputImage[curs] < media ? 0 : 255);
			} while (curs.MoveNext());
		}
	}

	//[AlgorithmInfo("27-09-2011 Es2", Category = "FEI")]
	public class BinaEConta : Algorithm {

		[AlgorithmInput]
		public Image<byte> InputImage { get; set; }
		[AlgorithmParameter]
		public MetricType Metric { get; set; }
		[AlgorithmOutput]
		public int componentiConnesse { get; set; }

		public override void Run() {
			byte foreground = 255;
			var binImage = new Binarizza(this.InputImage).Execute();
			var labeledImage = new ConnectedComponentsLabeling(binImage, foreground, Metric).Execute();

			int[] perimeters = new int[labeledImage.ComponentCount];
			ImageCursor cursor = new ImageCursor(labeledImage);
			do {
				if (labeledImage[cursor] != -1)
					if (labeledImage[cursor.North] == -1 || labeledImage[cursor.East] == -1 || labeledImage[cursor.South] == -1 || labeledImage[cursor.West] == -1)
						perimeters[labeledImage[cursor]]++;
			} while (cursor.MoveNext());

			for (int i = 0; i < labeledImage.ComponentCount; i++)
				if (perimeters[i] < 42)
					componentiConnesse++;
		}
	}
}