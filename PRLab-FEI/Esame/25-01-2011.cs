﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;
using BioLab.PatternRecognition.Localization;
using System.Diagnostics;

namespace PRLab.FEI {
	//[AlgorithmInfo("25-01-2011es1", Category = "FEI")]
	public class Es25012011 : ImageOperation<Image<byte>, Image<byte>> {
		[AlgorithmParameter]
		[DefaultValue(0)]
		public int DiscardPercentage { get; set; }

		public override void Run() {
			int max, min, diff;
			Histogram histogram = new HistogramBuilder(InputImage).Execute();

			if (DiscardPercentage != 0) {
				var range = (DiscardPercentage * InputImage.PixelCount) / 100;

				var sum = 0;

				min = 0;
				sum = histogram[0];
				while (sum <= range && min < 255) {
					sum += histogram[++min];
				}

				sum = 0;
				max = 255;
				sum = histogram[255];
				while (sum <= range && max > 0) {
					sum += histogram[--max];
				}
			} else {
				max = 0;
				min = 255;

				for (int i = 0; i < InputImage.PixelCount; i++) {
					if (InputImage[i] < min)
						min = InputImage[i];

					if (InputImage[i] > max)
						max = InputImage[i];
				}
			}

			diff = max - min;

			if (diff > 0)
				Result = new LookupTableTransform<byte>(InputImage, p => ((255 * (p - min)) / diff).ClipToByte()).Execute();
			else
				Result = InputImage.Clone();
		}
	}

	//[AlgorithmInfo("25-01-2011es2", Category = "FEI")]
	public class Es25012011es2 : ImageOperation<Image<byte>, Image<byte>> {
		[AlgorithmParameter]
		[DefaultValue(255)]
		public byte Foreground { get; set; }

		[AlgorithmOutput]
		public int MaxArea { get; set; }

		[AlgorithmOutput]
		public int ComponentNumber { get; set; }

		[AlgorithmOutput]
		public int MaxPerimeter { get; set; }

		[AlgorithmOutput]
		public int MinArea { get; set; }

		[AlgorithmOutput]
		public int AvarageArea { get; set; }

		public override void Run() {
			ConnectedComponentImage workImage = new ConnectedComponentsLabeling(InputImage, Foreground, MetricType.CityBlock).Execute();
			ComponentNumber = workImage.ComponentCount;
			int[] perimeters = new int[ComponentNumber];
			int[] areas = new int[ComponentNumber];

			ImageCursor cursor = new ImageCursor(workImage);
			do {
				if (workImage[cursor] != -1) {
					areas[workImage[cursor]]++;
					if (workImage[cursor.North] == -1 || workImage[cursor.East] == -1 || workImage[cursor.South] == -1 || workImage[cursor.West] == -1)
						perimeters[workImage[cursor]]++;
				}
			} while (cursor.MoveNext());

			MinArea = areas[0];
			MaxArea = areas[0];
			MaxPerimeter = perimeters[0];
			AvarageArea = areas[0];

			for (int i = 1; i < ComponentNumber; i++) {
				AvarageArea += areas[i];

				if (areas[i] < MinArea)
					MinArea = areas[i];

				if (areas[i] > MaxArea)
					MaxArea = areas[i];

				if (perimeters[i] > MaxPerimeter)
					MaxPerimeter = perimeters[i];
			}

			AvarageArea = AvarageArea / ComponentNumber;
		}
	}
}