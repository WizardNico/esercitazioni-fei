﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI {
	//[AlgorithmInfo("22-01-2011es1", Category = "FEI")]
	public class Colorize : ImageOperation<Image<byte>, RgbImage<byte>> {
		[AlgorithmParameter]
		public byte HueParam { get; set; }
		[AlgorithmParameter]
		public byte SaturationParam { get; set; }


		public override void Run() {
			//Creo un'immagine per ogni canale
			var ImgHue = new Image<byte>(InputImage.Width, InputImage.Height);
			var ImgSatur = new Image<byte>(InputImage.Width, InputImage.Height);
			var ImgLight = new Image<byte>(InputImage.Width, InputImage.Height);
			ImgHue = ImgSatur = ImgLight = InputImage.Clone();

			//Calcolo le sfumature della hue
			for (int i = 0; i < ImgHue.PixelCount; i++)
				ImgHue[i] = (ImgHue[i] + HueParam * 255 / 100).ClipToByte();

			//Calcolo le sfumature della saturazione
			for (int i = 0; i < ImgSatur.PixelCount; i++)
				ImgSatur[i] = (ImgSatur[i] + SaturationParam * 255 / 100).ClipToByte();

			var imgHSL = new HslImage<byte>(ImgHue, ImgSatur, ImgLight);
			Result = imgHSL.ToByteRgbImage();
		}
	}


	//[AlgorithmInfo("22-01-2011es2 ", Category = "FEI")]
	public class Es2Es : Algorithm {
		[AlgorithmInput]
		public Image<byte> InputImage { get; set; }

		[AlgorithmParameter]
		public byte Foreground { get; set; }
		[AlgorithmParameter]
		public MetricType Metric { get; set; }

		[AlgorithmOutput]
		public int componentiConnesseAreaMinima { get; set; }
		[AlgorithmOutput]
		public int componentiConnessePerimetroMinimo { get; set; }

		public override void Run() {
			ConnectedComponentImage workImage = new ConnectedComponentsLabeling(InputImage, Foreground, Metric).Execute();
			var ComponentNumber = workImage.ComponentCount;
			int[] perimeters = new int[ComponentNumber];
			int[] areas = new int[ComponentNumber];

			ImageCursor cursor = new ImageCursor(workImage);
			do {
				if (workImage[cursor] != -1) {
					areas[workImage[cursor]]++;
					if (workImage[cursor.North] == -1 || workImage[cursor.East] == -1 || workImage[cursor.South] == -1 || workImage[cursor.West] == -1)
						perimeters[workImage[cursor]]++;
				}
			} while (cursor.MoveNext());

			var areaMinima = 50;
			var perimetroMassimo = 20;

			for (int i = 0; i < ComponentNumber; i++) {
				if (areas[i] > areaMinima)
					componentiConnesseAreaMinima++;

				if (areas[i] < perimetroMassimo)
					componentiConnessePerimetroMinimo++;
			}
		}
	}
}