﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI {
	//[AlgorithmInfo("17-07-2012 Es1", Category = "FEI")]
	public class BinaryTransformer : TopologyOperation<Image<double>> {

		[AlgorithmParameter]
		public MetricType Metric { get; set; }

		public BinaryTransformer(Image<byte> inputImage) {
			this.InputImage = inputImage;
		}

		public BinaryTransformer() {
		}

		public override void Run() {
			var workImage = new ConnectedComponentsLabeling(InputImage, Foreground, Metric).Execute();

			int[] perimeters = new int[workImage.ComponentCount];
			int[] areas = new int[workImage.ComponentCount];

			//Calcolo aree e perimetri, quest'ultimo in base alla metrica che ho scelto
			ImageCursor cursor = new ImageCursor(workImage);
			do {
				if (workImage[cursor] != -1) {
					areas[workImage[cursor]]++;

					if (Metric == MetricType.CityBlock) {
						if (workImage[cursor.North] == -1 || workImage[cursor.East] == -1 || workImage[cursor.South] == -1 || workImage[cursor.West] == -1)
							perimeters[workImage[cursor]]++;
					} else {
						if (workImage[cursor.North] == -1 || workImage[cursor.Northeast] == -1 || workImage[cursor.East] == -1 || workImage[cursor.Southeast] == -1 || workImage[cursor.South] == -1 || workImage[cursor.Southwest] == -1 || workImage[cursor.West] == -1 || workImage[cursor.Northwest] == -1)
							perimeters[workImage[cursor]]++;
					}
				}
			} while (cursor.MoveNext());

			cursor.Restart();
			Result = new Image<double>(InputImage.Width, InputImage.Height);


			//mi salvo tutti già i rapporti per non doverlo ricalcolare ogni volta
			double[] rapporto = new double[workImage.ComponentCount];
			for (int i = 0; i < workImage.ComponentCount; i++)
				if (perimeters[i] != 0)
					rapporto[i] = Convert.ToDouble(areas[i] / perimeters[i]);


			//Calcolo i valori della nuova immagine
			do {
				if (workImage[cursor] != -1)
					Result[cursor] = rapporto[workImage[cursor]];
				else
					Result[cursor] = 0;
			} while (cursor.MoveNext());
		}
	}

	//[AlgorithmInfo("17-07-2012 Es2", Category = "FEI")]
	public class es5_17lugl12 : TopologyOperation<Image<byte>> {
		[AlgorithmParameter]
		public MetricType Metric { get; set; }

		public override void Run() {
			var workImage = new ConnectedComponentsLabeling(InputImage, Foreground, Metric).Execute();

			int[] perimeters = new int[workImage.ComponentCount];
			int[] areas = new int[workImage.ComponentCount];

			//Calcolo aree e perimetri, quest'ultimo in base alla metrica che ho scelto
			ImageCursor cursor = new ImageCursor(workImage);
			do {
				if (workImage[cursor] != -1) {
					areas[workImage[cursor]]++;

					if (Metric == MetricType.CityBlock) {
						if (workImage[cursor.North] == -1 || workImage[cursor.East] == -1 || workImage[cursor.South] == -1 || workImage[cursor.West] == -1)
							perimeters[workImage[cursor]]++;
					} else {
						if (workImage[cursor.North] == -1 || workImage[cursor.Northeast] == -1 || workImage[cursor.East] == -1 || workImage[cursor.Southeast] == -1 || workImage[cursor.South] == -1 || workImage[cursor.Southwest] == -1 || workImage[cursor.West] == -1 || workImage[cursor.Northwest] == -1)
							perimeters[workImage[cursor]]++;
					}
				}
			} while (cursor.MoveNext());


			//mi salvo tutti già i rapporti per non doverlo ricalcolare ogni volta
			int[] rapporto = new int[workImage.ComponentCount];
			int media = 0;
			for (int i = 0; i < workImage.ComponentCount; i++) {
				if (perimeters[i] != 0) {
					rapporto[i] = areas[i] / perimeters[i];
					media += rapporto[i];
				}
			}
			media /= workImage.ComponentCount;

			//Precalcolo quali sono le componenti da nascondere e quali no
			for (int i = 0; i < workImage.ComponentCount; i++)
				if (rapporto[i] <= media)
					rapporto[i] = -1;

			cursor.Restart();
			do {
				if (workImage[cursor] != -1 && rapporto[workImage[cursor]] == -1)
					workImage[cursor] = -1;
			} while (cursor.MoveNext());

			Result = workImage.Clone().ToByteImage();
		}
	}
}