﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using BioLab.Common;
using BioLab.GUI.Forms;
using BioLab.ImageProcessing;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI {
	//[AlgorithmInfo("18-09-2012 Es1", Category = "FEI")]
	public class InfoImg : TopologyOperation<Image<double>> {

		[AlgorithmParameter]
		public MetricType Metric { get; set; }

		public InfoImg(Image<byte> inputImage) {
			this.InputImage = inputImage;
		}

		public InfoImg() {
		}

		public override void Run() {
			var ImgWork = new ConnectedComponentsLabeling(InputImage, Foreground, Metric).Execute();

			int[] perimeters = new int[ImgWork.ComponentCount];
			int[] areasWithoutPerimeters = new int[ImgWork.ComponentCount];
			double[] rapporto = new double[ImgWork.ComponentCount];

			var cursor = new ImageCursor(ImgWork);

			//Considero solo perimetri e pixel di riempimento
			do {
				if (ImgWork[cursor] != -1) {
					areasWithoutPerimeters[ImgWork[cursor]]++;
					if (Metric == MetricType.CityBlock) {
						if (ImgWork[cursor.North] == -1 || ImgWork[cursor.East] == -1 || ImgWork[cursor.South] == -1 || ImgWork[cursor.West] == -1) {
							perimeters[ImgWork[cursor]]++;
							areasWithoutPerimeters[ImgWork[cursor]]--;
						}
					} else {
						if (ImgWork[cursor.North] == -1 || ImgWork[cursor.Northeast] == -1 || ImgWork[cursor.East] == -1 || ImgWork[cursor.Southeast] == -1 || ImgWork[cursor.South] == -1 || ImgWork[cursor.Southwest] == -1 || ImgWork[cursor.West] == -1 || ImgWork[cursor.Northwest] == -1) {
							perimeters[ImgWork[cursor]]++;
							areasWithoutPerimeters[ImgWork[cursor]]--;
						}
					}
				}
			} while (cursor.MoveNext());

			Result = InputImage.ToDoubleImage().Clone();

			//Calcolo i rapporti e di conseguenza il risultato
			cursor.Restart();
			do {
				if (ImgWork[cursor] != -1)
					Result[cursor] = Convert.ToDouble(ImgWork[cursor] / Result[cursor]);
				else
					Result[cursor] = 0;
			} while (cursor.MoveNext());
		}
	}

	//[AlgorithmInfo("18-09-2012 Es2", Category = "FEI")]
	public class es2 : TopologyOperation<Image<byte>> {
		[AlgorithmParameter]
		public MetricType Metric { get; set; }

		public override void Run() {
			var ImgWork = new ConnectedComponentsLabeling(InputImage, Foreground, Metric).Execute();

			int[] perimeters = new int[ImgWork.ComponentCount];
			int[] areasWithoutPerimeters = new int[ImgWork.ComponentCount];
			int[] rapporto = new int[ImgWork.ComponentCount];

			var cursor = new ImageCursor(ImgWork);

			//Considero solo perimetri e pixel di riempimento
			do {
				if (ImgWork[cursor] != -1) {
					areasWithoutPerimeters[ImgWork[cursor]]++;
					if (Metric == MetricType.CityBlock) {
						if (ImgWork[cursor.North] == -1 || ImgWork[cursor.East] == -1 || ImgWork[cursor.South] == -1 || ImgWork[cursor.West] == -1) {
							perimeters[ImgWork[cursor]]++;
							areasWithoutPerimeters[ImgWork[cursor]]--;
						}
					} else {
						if (ImgWork[cursor.North] == -1 || ImgWork[cursor.Northeast] == -1 || ImgWork[cursor.East] == -1 || ImgWork[cursor.Southeast] == -1 || ImgWork[cursor.South] == -1 || ImgWork[cursor.Southwest] == -1 || ImgWork[cursor.West] == -1 || ImgWork[cursor.Northwest] == -1) {
							perimeters[ImgWork[cursor]]++;
							areasWithoutPerimeters[ImgWork[cursor]]--;
						}
					}
				}
			} while (cursor.MoveNext());

			//Calcolo il rapporto tra numero pixel interni e perimetro su tutte le componenti
			int media = 0;
			for (int i = 0; i < ImgWork.ComponentCount; i++) {
				if (perimeters[i] != 0) {
					rapporto[i] = areasWithoutPerimeters[i] / perimeters[i];
					media += rapporto[i];
				}
			}
			media /= ImgWork.ComponentCount;

			//Precalcolo quali sono le componenti da nascondere e quali no
			for (int i = 0; i < ImgWork.ComponentCount; i++)
				if (rapporto[i] <= media)
					rapporto[i] = -1;

			//Vado a correggere l'immagine Result
			cursor.Restart();
			do {
				if (ImgWork[cursor] != -1 && rapporto[ImgWork[cursor]] == -1)
					ImgWork[cursor] = -1;
			} while (cursor.MoveNext());

			Result = ImgWork.Clone().ToByteImage();
		}
	}
}