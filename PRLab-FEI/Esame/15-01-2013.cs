﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;
using BioLab.PatternRecognition.Localization;
using System.Diagnostics;

namespace PRLab.FEI {
	//[AlgorithmInfo("15-01-2013", Category = "FEI")]
	public class EsercizioLung : ImageOperation<Image<byte>, Image<byte>> {
		[AlgorithmOutput]
		public Image<int> ResultX { get; set; }
		[AlgorithmOutput]
		public Image<int> ResultY { get; set; }
		[AlgorithmOutput]
		public Image<byte> Result1 { get; set; }
		[AlgorithmOutput]
		public Image<double> Result2 { get; set; }
		[AlgorithmOutput]
		public Image<byte> Result3 { get; set; }
		[AlgorithmOutput]
		public ConnectedComponentImage Result4 { get; set; }
		[AlgorithmOutput]
		public ConnectedComponentImage Result5 { get; set; }

		public override void Run() {
			/*
			 * ------------------------------------(1)------------------------------------
			 * Calcolare, per ogni pixel di InputImage (esclusi eventualmente quelli di bordo), 
			 * le componenti x e y del gradiente mediante convoluzione con opportuni filtri.
			 */
			Result1 = InputImage.Clone();
			//Creo i filtri di prewitt
			int[] valoriX = new int[] { 1, 0, -1, 1, 0, -1, 1, 0, -1 };
			int[] valoriY = new int[] { 1, 1, 1, 0, 0, 0, -1, -1, -1 };

			//Calcolo la convoluzione scartando un pixel di bordo
			ResultX = new ByteToIntConvolution(InputImage, new ConvolutionFilter<int>(valoriX, 3), 1).Execute();
			ResultY = new ByteToIntConvolution(InputImage, new ConvolutionFilter<int>(valoriY, 3), 1).Execute();

			//Calcolo il gradiente
			for (int i = 0; i < InputImage.PixelCount; i++)
				Result1[i] = (byte)Math.Sqrt(Math.Pow(ResultX[i], 2) + Math.Pow(ResultY[i], 2));



			/*
			 * ------------------------------------(2)------------------------------------
			 * Calcolare, per ogni pixel di InputImage (esclusi eventualmente quelli di bordo), 
			 * l’orientazione del gradiente, esprimendola come angolo in radianti in [-π,π].
			 */
			Result2 = new Image<double>(InputImage.Width, InputImage.Height);
			for (int i = 0; i < InputImage.PixelCount; i++)
				Result2[i] = Math.Atan2(ResultY[i], ResultX[i]);



			/*
			 * ------------------------------------(3)------------------------------------
			 * Binarizzare InputImage utilizzando una soglia globale pari al livello medio di grigio dell’immagine.
			 */
			Result3 = new Image<byte>(InputImage.Width, InputImage.Height);
			int media = 0;
			ImageCursor cursore = new ImageCursor(InputImage);
			do {
				media += InputImage[cursore];
			} while (cursore.MoveNext());
			media /= InputImage.PixelCount;
			cursore.Restart();
			do {
				Result3[cursore] = (byte)(InputImage[cursore] < media ? 0 : 255);
			} while (cursore.MoveNext());



			/*
			 * ------------------------------------(4)------------------------------------
			 * Etichettare le componenti connesse dell’immagine binarizzata, utilizzando 255 come valore di foreground.
			 */
			Result4 = new ConnectedComponentsLabeling(Result3, 255, MetricType.Chessboard).Execute();



			/*
			 * ------------------------------------(5)------------------------------------
			 * Individuare eventuali componenti connesse che soddisfino la seguente condizione: 
			 * almeno il 15% dei pixel ha orientazione del gradiente (così come calcolata su InputImage) compresa nell’intervallo (-π/4,π/4).
			 */
			Result5 = Result4.Clone();
			cursore.Restart();
			int[] somma = new int[Result5.ComponentCount];
			int[] areas = new int[Result5.ComponentCount];
			do {
				if (Result5[cursore] != -1) {
					areas[Result5[cursore]]++;
					if (Result2[cursore] < Math.PI / 4 && Result2[cursore] > (-1) * Math.PI / 4)
						somma[Result5[cursore]]++;
				}
			} while (cursore.MoveNext());

			cursore.Restart();
			do {
				if ((Result5[cursore] != -1) && (somma[Result5[cursore]] < areas[Result5[cursore]] * 0.15))
					Result5[cursore] = -1;
			} while (cursore.MoveNext());



			/*
			 * ------------------------------------(6)------------------------------------
			 * Restituire come output un’immagine grayscale (Result) in cui i pixel appartenenti alle componenti connesse individuate 
			 * al passo precedente corrispondono a quelli di InputImage, mentre tutti gli altri pixel hanno valore zero.
			 */
			Result = new Image<byte>(InputImage.Width, InputImage.Height);
			cursore.Restart();
			do {
				if (Result5[cursore] != -1)
					Result[cursore] = InputImage[cursore];
			} while (cursore.MoveNext());
		}
	}
}