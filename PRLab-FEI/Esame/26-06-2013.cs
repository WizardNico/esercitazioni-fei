﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;
using BioLab.PatternRecognition.Localization;
using System.Diagnostics;

namespace PRLab.FEI {
	//[AlgorithmInfo("26-06-2013 Es1", Category = "FEI")]
	public class Es1del2013 : ImageOperation<Image<byte>, Image<byte>> {
		[AlgorithmOutput]
		public Image<byte> Ris1 { get; set; }
		[AlgorithmOutput]
		public Image<byte> Diff { get; set; }
		[AlgorithmOutput]
		public Image<byte> Ris3 { get; set; }
		[AlgorithmOutput]
		public Image<byte> Ris4 { get; set; }
		[AlgorithmOutput]
		public ConnectedComponentImage Ris5 { get; set; }
		[AlgorithmParameter]
		[DefaultValue(MetricType.Chessboard)]
		public MetricType Metric { get; set; }

		//esercizio 1, calcolare media intorno 51x51
		public override void Run() {
			int media = 0;
			Ris1 = new Image<byte>(InputImage.Width, InputImage.Height);

			ImageCursor cursor = new ImageCursor(InputImage);
			do {
				//Prendo il range di righe da scorrere
				int i1 = Math.Max(0, cursor.Y - 51);
				int i2 = Math.Min(InputImage.Height, cursor.Y + 51);

				//Prendo il range di colonne da scorrere
				int j1 = Math.Max(0, cursor.X - 51);
				int j2 = Math.Min(InputImage.Width, cursor.X + 51);

				for (int i = i1; i < i2; i++)
					for (int j = j1; j < j2; j++)
						media += InputImage[i, j];

				media = media / 2601;
				Ris1[cursor] = media.ClipToByte();
			} while (cursor.MoveNext());



			//punto 2
			cursor.Restart();
			Diff = new Image<byte>(Ris1.Width, Ris1.Height);
			do {
				Diff[cursor] = (Math.Abs(InputImage[cursor] - Ris1[cursor])).ClipToByte();
			} while (cursor.MoveNext());



			//punto  3
			//Calcolo il numero di pixel da scartare
			Ris3 = new Image<byte>(InputImage.Width, InputImage.Height);
			int nDiscardPixel = (2 * Diff.PixelCount) / 100;

			//Setto il minimo e il massimo
			int min = 0;
			int max = 255;
			int sum;

			//Creo l'istogramma
			Histogram histogram = new HistogramBuilder(Diff).Execute();

			//Posizione il mio contatore all'inizio, sul lato sinistro, dell'istogramma
			sum = histogram[min];

			/*
			 * Scorro tutto il vettore in avanti fintanto che min<255 e ho scartato la percentuale giusta di pixel a sinistra.
			 * Vado in sostanza a riposizionare correttamente la variabile min.		
			 * Nota che uso sum += histogram[++min]; perchè ogni cella dell'istogramma al suo interno ha un numero di pixel quindi io devo 
			 * tenere traccia di quanti ne scarto confrontado ogni volta con nDiscardPixel
			 */
			while ((min < 255) && (sum <= nDiscardPixel)) {
				sum += histogram[++min];
			}

			//Posiziono il mio contatore ora sul lato destro dell'istogramma 
			sum = histogram[max];

			//Ora procedo all'indietro fintanto che max>0 e ho scartato la percentuale giusta di pixel a destra
			//Vado ora a riposizionare la variabile max
			while ((max > 0) && (sum <= nDiscardPixel)) {
				sum += histogram[--max];
			}

			int diff = max - min;
			if (diff > 0) {
				var op = new LookupTableTransform<byte>(Diff, p => (255 * (p - min) / diff).ClipToByte());
				Ris3 = op.Execute();
			} else {
				Ris3 = InputImage.Clone();
			}



			//punto 4
			cursor.Restart();
			Ris4 = new Image<byte>(InputImage.Width, InputImage.Height);
			do {
				Ris4[cursor] = (Diff[cursor] > 128 ? 255 : 0).ClipToByte();
			} while (cursor.MoveNext());



			//punto5
			Ris5 = new ConnectedComponentsLabeling(Ris4, 255, MetricType.CityBlock).Execute();
			ImageCursor comp = new ImageCursor(Ris5);
			int[] perimetro = new int[Ris5.ComponentCount];
			do {
				if (Ris5[comp] != -1 && perimetro[Ris5[comp]] != -1) {
					if (Ris5[comp.North] == -1 || Ris5[comp.South] == -1 || Ris5[comp.West] == -1 || Ris5[comp.East] == -1) {
						perimetro[Ris5[comp]]++;
						if (perimetro[Ris5[comp]] > 25) {
							perimetro[Ris5[comp]] = -1;
						}
					}
				}

			} while (comp.MoveNext());
			comp.Restart();
			do {
				if (Ris5[comp] != -1 && perimetro[Ris5[comp]] == -1) {
					Ris5[comp] = -1;
				}
			} while (comp.MoveNext());



			//punto 6
			Result = new Image<byte>(Ris5.Width, Ris5.Height);
			comp.Restart();
			do {
				Result[comp] = (Ris5[comp] != -1 ? 255 : 0).ClipToByte();
			} while (comp.MoveNext());
		}
	}
}